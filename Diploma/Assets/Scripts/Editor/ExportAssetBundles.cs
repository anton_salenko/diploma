using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class ExportAssetBundles
{
    static Dictionary<string, string> FilesOnBundles = new Dictionary<string, string>();
    [System.Serializable]
    private class MaterialShader
    {
        public string material;
        public string shader;

        public MaterialShader(string material, string shader)
        {
            this.material = material;
            this.shader = shader;
        }
    }

    [System.Serializable]
    private class MaterialShaderInfo
    {
        public List<MaterialShader> materials = new List<MaterialShader>();
    }
    ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  
    ///  
    [MenuItem("Assets/AssetBundles/Build Android and iOS Compression")]
    static void AssetsBuildAllAssetBundlesAllChunkBasedCompression()
    {
        BuildAllAssetBundlesAllChunkBasedCompression();
    }

    [MenuItem("Assets/AssetBundles/Build Android and iOS Uncompressed")]
    static void AssetsBuildAllAssetBundlesAllChunkBasedUncompressed()
    {
        BuildAllAssetBundlesAllUncompressed();
    }
    [MenuItem("Assets/AssetBundles/AssetBundles Clear name")]
    static void AssetsAssetBundlesClearname()
    {
        AssetBundlesClearname();
    }

    [MenuItem("Build AssetBundles/Build Android and iOS Compression")]
    static void BuildAllAssetBundlesAllChunkBasedCompression()
    {
        FilesOnBundles = new Dictionary<string, string>();
        string assetBundleDirectory = "Assets/AssetBundles";
        if (Directory.Exists(assetBundleDirectory))
        {
            Directory.Delete(assetBundleDirectory, true);
        }

        BuildTarget bt = EditorUserBuildSettings.activeBuildTarget;
        if (bt == BuildTarget.Android)
        {
            BuildAllAssetBundles(BuildTarget.Android, BuildAssetBundleOptions.ChunkBasedCompression);
            BuildAllAssetBundles(BuildTarget.iOS, BuildAssetBundleOptions.ChunkBasedCompression);
        }
        else if (bt == BuildTarget.iOS)
        {
            BuildAllAssetBundles(BuildTarget.iOS, BuildAssetBundleOptions.ChunkBasedCompression);
            BuildAllAssetBundles(BuildTarget.Android, BuildAssetBundleOptions.ChunkBasedCompression);
        }
    }

    [MenuItem("Build AssetBundles/Build Android and iOS Uncompressed")]
    static void BuildAllAssetBundlesAllUncompressed()
    {
        FilesOnBundles = new Dictionary<string, string>();
        string assetBundleDirectory = "Assets/AssetBundles";
        if (Directory.Exists(assetBundleDirectory))
        {
            Directory.Delete(assetBundleDirectory, true);
        }

        BuildTarget bt = EditorUserBuildSettings.activeBuildTarget;
        if (bt == BuildTarget.Android)
        {
            BuildAllAssetBundles(BuildTarget.Android, BuildAssetBundleOptions.UncompressedAssetBundle);
            BuildAllAssetBundles(BuildTarget.iOS, BuildAssetBundleOptions.UncompressedAssetBundle);
        }
        else if (bt == BuildTarget.iOS)
        {
            BuildAllAssetBundles(BuildTarget.iOS, BuildAssetBundleOptions.UncompressedAssetBundle);
            BuildAllAssetBundles(BuildTarget.Android, BuildAssetBundleOptions.UncompressedAssetBundle);
        }
    }

    [MenuItem("Build AssetBundles/Build Current Target Compression")]
    static void BuildCurrentTargetAssetBundlesAllChunkBasedCompression()
    {
        FilesOnBundles = new Dictionary<string, string>();
        string assetBundleDirectory = "Assets/AssetBundles";
        if (Directory.Exists(assetBundleDirectory))
        {
            Directory.Delete(assetBundleDirectory, true);
        }

        BuildTarget bt = EditorUserBuildSettings.activeBuildTarget;

        BuildAllAssetBundles(bt, BuildAssetBundleOptions.ChunkBasedCompression);

    }

    [MenuItem("Build AssetBundles/AssetBundles Clear name")]
    static void AssetBundlesClearname()
    {
        if (Selection.assetGUIDs.Length > 0)
        {
            SelectionObjectsList = new List<Object>(Selection.objects);

            foreach (Object asset in SelectionObjectsList)
            {
                string path = AssetDatabase.GetAssetPath(asset);
                AssetImporter assetImporter = AssetImporter.GetAtPath(path);
                assetImporter.assetBundleName = "";
            }
        }
    }

    ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  

    static List<Object> SelectionObjectsList = null;

    static bool SetAssetBundlesFileNames(BuildTarget buildTarget)
    {
        bool ret = false;
        if (Selection.assetGUIDs.Length > 0)
        {
            SelectionObjectsList = new List<Object>(Selection.objects);

            foreach (Object asset in SelectionObjectsList)
            {
                ret = true;
                string path = AssetDatabase.GetAssetPath(asset);

                Renderer[] renderers = ((GameObject)asset).GetComponentsInChildren<Renderer>(true);
                MaterialShaderInfo materialShaderInfo = new MaterialShaderInfo();
                Debug.Log("renderers.count = " + renderers.Length);
                foreach (Renderer renderer in renderers)
                {

                    Debug.Log("renderer.sharedMaterials.Length = " + renderer.sharedMaterials.Length);
                    foreach (Material material in renderer.sharedMaterials)
                    {
                        Debug.Log(material);
                        if (material != null)
                        {
                            string materialName = material.name;
                            string shaderName = material.shader.name;
                            materialShaderInfo.materials.Add(new MaterialShader(materialName, shaderName));
                        }
                    }
                }

                string json = EditorJsonUtility.ToJson(materialShaderInfo);

                Debug.Log(json);

                TextAsset textAsset = ConvertStringToTextAsset(json);

                AssetImporter assetImporterJson = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(textAsset));
                assetImporterJson.assetBundleName = asset.name + "_" + buildTarget.ToString();

                AssetImporter assetImporter = AssetImporter.GetAtPath(path);
                assetImporter.assetBundleName = asset.name + "_" + buildTarget.ToString();
            }
        }
        else
        {
            Debug.LogError("No Assets Selected");
        }
        return ret;
    }

    static bool ResetSetAssetBundlesFileNames()
    {
        bool ret = false;
        foreach (Object asset in SelectionObjectsList)
        {
            ret = true;
            string path = AssetDatabase.GetAssetPath(asset);
            AssetImporter assetImporter = AssetImporter.GetAtPath(path);
            assetImporter.assetBundleName = "";
        }
        return ret;
    }

    static void BuildAllAssetBundles(BuildTarget buildTarget, BuildAssetBundleOptions compression = BuildAssetBundleOptions.ChunkBasedCompression)
    {
        bool ret = SetAssetBundlesFileNames(buildTarget);

        if (ret)
        {
            AssetBundleManifest abm = BuildPipeline.BuildAssetBundles(GetAssetBundleDirectory(buildTarget), compression, buildTarget);
        }
        else
        {
            Debug.LogError("PLS Selected Prefabs");
        }

        ResetSetAssetBundlesFileNames();

    }

    static TextAsset ConvertStringToTextAsset(string text)
    {
        string temporaryTextFileName = "MaterialShaderInfo";

        if (Directory.Exists(Application.dataPath + "/Resources/") == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/");
        }
        File.WriteAllText(Application.dataPath + "/Resources/" + temporaryTextFileName + ".txt", text);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        TextAsset textAsset = Resources.Load(temporaryTextFileName) as TextAsset;
        return textAsset;
    }


    static string GetAssetBundleDirectory(BuildTarget buildTarget)
    {

        string assetBundleDirectory = "Assets/AssetBundles";

        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }


        if (!Directory.Exists(assetBundleDirectory + "/" + buildTarget.ToString()))
        {
            Directory.CreateDirectory(assetBundleDirectory + "/" + buildTarget.ToString());
        }

        return assetBundleDirectory + "/" + buildTarget.ToString();
    }
}