﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class DeviceTools : MonoBehaviour
{
	public static Rect ScreenRect( float left, float top, float width, float height )
	{
		float W = Screen.width - 1.0f;
		float H = Screen.height - 1.0f;
		return new Rect( W * left, H * top, W * width, H * height );
	}

	public static bool isDevice()
	{
#if UNITY_EDITOR || UNITY_STANDALONE
#if UNITY_EDITOR
		if( UnityEditor.EditorApplication.isRemoteConnected )
		{
			return true;
		}
		else
#endif
		{
			return false;
		}
#else
		return true;
#endif
	}
}
