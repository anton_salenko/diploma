﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager
{
    private string SOUND_KEY = "sound";
    private string MUSIC_KEY = "music";

    public float SoundVolume
    {
        get
        {
            return PlayerPrefs.GetInt(SOUND_KEY, 1);
        }

        set
        {
            PlayerPrefs.SetInt(SOUND_KEY, Mathf.RoundToInt(Mathf.Clamp01(value)));
        }
    }

    public float MusicVolume
    {
        get
        {
            return PlayerPrefs.GetInt(MUSIC_KEY, 1);
        }

        set
        {
            PlayerPrefs.SetInt(MUSIC_KEY, Mathf.RoundToInt(Mathf.Clamp01(value)));
        }
    }

}
