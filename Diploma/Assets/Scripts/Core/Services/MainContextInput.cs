﻿//#define DEBUG_LOG
//#define DEBUG_LOG_GUI

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.mediation.api;
using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine.EventSystems;

// Player Input
public class MainContextInput   : MainContextRoot
{
	public void OnApplicationFocus( bool focus ) { }

	public MainContextInput( MonoBehaviour contextView ):base( contextView )
	{
	}

	static Touch[] touchClear1 = new Touch[5];
	static Touch[] touchClear2 = new Touch[5];


	public override IContext Start()
	{
		IContext c =  base.Start();

		for( int i = 0; i <= 4; i++ )
		{
			touchClear1[i] = new Touch();
		}
		for( int i = 0; i <= 4; i++ )
		{
			touchClear2[i] = new Touch();
		}

		if( dispatcher != null )
		{
			//	dispatcher.AddListener(EventGlobal.E_ResetMainContextInput, ResetMainContextInput);
		}
		else
		{
            Debug.LogError( "dispatcher == NULL E_ResetMainContextInput ERROR" );
		}

		ResetMainContextInput( null );
		return c;
	}

	public void Update()
	{
		if( dispatcher != null )
		{
			// Android beack button
			if( Input.GetKeyDown( KeyCode.Escape ) )
			{
                dispatcher.Dispatch( Events.E_AppBackButton );
			}
			//
			UpdateMainContextInput();
			//	dispatcher.Dispatch(EventGlobal.E_AppUpdate, Time.deltaTime);
		}
		else
		{
            Debug.LogError( "Update ERROR!!! dispatcher == null" );
		}
	}

	public void FixedUpdate()
	{
		if( dispatcher != null )
		{
			//	dispatcher.Dispatch(EventGlobal.E_AppFixedUpdate, Time.deltaTime);
		}
		else
		{
            Debug.LogError( "FixedUpdate ERROR!!! dispatcher == null" );
		}
	}

	public void LateUpdate()
	{
		if( dispatcher != null )
		{
			//	dispatcher.Dispatch(EventGlobal.E_AppLateUpdate, Time.deltaTime);
		}
		else
		{
            Debug.LogError( "LateUpdate ERROR!!! dispatcher == null" );
		}
	}

	/// ///////////////////////////////////////////////////////// Touch system
	public enum stateTouch
	{
		STdefault = 0,
		STZoomStart,
		STZoom,
		STZoomEnd,
		STSwipeStart,
		STSwipe,
		STSwipeEnd,
		STDragStart,
		STDrag,
		STDragEnd,
	}
	stateTouch state = stateTouch.STdefault;

	Touch[] touchOne = null;
	int touchOneFingerID = -1;
	float touchOneStateTime = 0;
	float touchOneStateMoveTime = 0;
	Touch[] touchTwo = null;
	int touchTwoFingerID = -1;
	// float DragTime = -0.5f;

#if DEBUG_LOG_GUI
	string touchDebug = "";
	stateTouch[] stateOld = new stateTouch[5];
	GUIStyle guinullstile = new GUIStyle();
#endif
	public void OnGUI()
	{
#if DEBUG_LOG_GUI
		guinullstile.normal.textColor = Color.magenta;
		guinullstile.fontStyle = FontStyle.Bold;
		guinullstile.fontSize = ( int )( Screen.height * 0.020f );

		GUI.Box( DeviceTools.ScreenRect( 0.1f, 0.00f, 1f, 0.025f ),
				 touchDebug, guinullstile );
#endif
	}

	void ResetMainContextInput( IEvent evt )
	{
	}

	public void UpdateMainContextInput()
	{


		if( DeviceTools.isDevice() )
		{
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////// Device
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			updateTouchArray();
			state = handlerZoom( state );
			state = handlerDrag( state );
			state = handlerSwipe( state );
#if DEBUG_LOG_GUI
			touchDebug = "";
			if( stateOld[0] != state )
			{
				stateOld[4] = stateOld[3];
				stateOld[3] = stateOld[2];
				stateOld[2] = stateOld[1];
				stateOld[1] = stateOld[0];
				stateOld[0] = state;
			}

			touchDebug += state.ToString();
			if( state != stateTouch.STdefault )
			{
				touchDebug += " " + stateOld[1].ToString() + " " + stateOld[2].ToString()
							  + " " + stateOld[3].ToString()
							  + " " + stateOld[4].ToString() + "\n";
			}
			if( touchOne != null )
			{
				touchDebug += " 1T0:" + touchOne[0].phase.ToString()[0] +
							  " 1T1:" + touchOne[1].phase.ToString()[0] +
							  " 1T2:" + touchOne[2].phase.ToString()[0] +
							  " 1T3:" + touchOne[3].phase.ToString()[0] +
							  " 1T4:" + touchOne[4].phase.ToString()[0] + "\n"; ;
			}
			if( touchTwo != null )
			{
				touchDebug += " 2T0:" + touchTwo[0].phase.ToString()[0] +
							  " 2T1:" + touchTwo[1].phase.ToString()[0] +
							  " 2T2:" + touchTwo[2].phase.ToString()[0] +
							  " 2T3:" + touchTwo[3].phase.ToString()[0] +
							  " 2T4:" + touchTwo[4].phase.ToString()[0] + "\n"; ;
			}
#endif

			if( state == stateTouch.STZoom || state == stateTouch.STZoomEnd || state == stateTouch.STZoomStart )
			{
				Vector3 point1 = Vector3.zero;
				Vector3 point2 = Vector3.zero;
				Vector3 point1Delta = Vector3.zero;
				Vector3 point2Delta = Vector3.zero;
				if( touchOne != null )
				{
					point1 = touchOne[0].position;
					point1Delta = touchOne[0].deltaPosition;
				}
				if( touchTwo != null )
				{
					point2 = touchTwo[0].position;
					point2Delta = touchTwo[0].deltaPosition;
				}
				ZoomEvent( state, point1, point2, point1Delta, point2Delta );
			}
			else if( state == stateTouch.STDrag || state == stateTouch.STDragEnd || state == stateTouch.STDragStart )
			{
				Vector3 point1 = Vector3.zero;
				Vector3 point1Delta = Vector3.zero;
				if( touchOne != null )
				{
					point1 = touchOne[0].position;
					point1Delta = touchOne[0].deltaPosition;
				}
				DragEvent( state, point1, point1Delta, touchOneStateTime );
			}
			else if( state == stateTouch.STSwipe || state == stateTouch.STSwipeEnd || state == stateTouch.STSwipeStart )
			{
				Vector3 point1 = Vector3.zero;
				Vector3 point1Delta = Vector3.zero;
				if( touchOne != null )
				{
					point1 = touchOne[0].position;
					point1Delta = touchOne[0].deltaPosition;
				}
				SwipeEvent( state, point1, point1Delta, touchOneStateTime );
			}
			// Tap event
			if( handlerTap() )
			{
				Vector3 point1 = Vector3.zero;
				int tapCount = 0;
				if( touchOne != null )
				{
					point1 = touchOne[0].position;
					tapCount = touchOne[0].tapCount;
					TapEvent( state, point1, tapCount, TestDragObjectPosition( point1 ) );
				}
			}

			// Hold Event
			// Tap event
			if( handlerHold() )
			{
				Vector3 point1 = Vector3.zero;
				if( touchOne != null )
				{
					point1 = touchOne[0].position;
					HoldEvent( state, point1, touchOneStateTime, TestDragObjectPosition( point1 ) );
				}
			}
		}
		else
		{
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////// EDITOR / STANDALONE emulation
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			updateMouseEmulationTouchState();
			if( state == stateTouch.STZoom || state == stateTouch.STZoomEnd || state == stateTouch.STZoomStart )
			{
				if( state == stateTouch.STZoomStart )
				{
					mouseOldzoom = 200;
				}
				mouseOldzoom += ( Input.GetAxis( "Mouse ScrollWheel" ) * 100 );
				Vector3 point1Delta = new Vector3( 0, ( Input.GetAxis( "Mouse ScrollWheel" ) * 100 ), 0 );
				Vector3 point2Delta = new Vector3( 0, -( Input.GetAxis( "Mouse ScrollWheel" ) * 100 ), 0 );
				Vector3 point1 = Input.mousePosition + new Vector3( 0, mouseOldzoom, 0 );
				Vector3 point2 = Input.mousePosition + new Vector3( 0, -mouseOldzoom, 0 );
				ZoomEvent( state, point1, point2, point1Delta, point2Delta );
			}

			else if( state == stateTouch.STDrag || state == stateTouch.STDragEnd || state == stateTouch.STDragStart )
			{
				if( state == stateTouch.STDragStart )
				{
					mouseOldPosition = Input.mousePosition;
				}

				Vector3 point1 = Input.mousePosition;
				Vector3 point1Delta = mouseOldPosition - Input.mousePosition;
				DragEvent( state, point1, -point1Delta, touchOneStateTime );
			}
			else if( state == stateTouch.STSwipe || state == stateTouch.STSwipeEnd || state == stateTouch.STSwipeStart )
			{
				Vector3 point1 = Input.mousePosition;
				Vector3 point1Delta = mouseOldPosition - Input.mousePosition;
				SwipeEvent( state, point1, -point1Delta, touchOneStateTime );
			}

			// Tap event
			bool isHoveringUI = checkHoveringUI( Input.mousePosition );


			if( Input.GetMouseButton( 0 ) && !mouseOldStatus && !isHoveringUI )
			{
				Vector3 point1 = Input.mousePosition;
				int tapCount = 1;
				TapEvent( state, point1, tapCount, TestDragObjectPosition( point1 ) );
			}

			// Hold Event
			// Tap event
			if( Input.GetMouseButton( 0 ) && mouseOldStatus == Input.GetMouseButton( 0 ) && !isHoveringUI )
			{
				Vector3 point1 = Input.mousePosition;
				touchOneStateTime += Time.deltaTime;
				HoldEvent( state, point1, touchOneStateTime, TestDragObjectPosition( point1 ) );
			}
			else
			{
				touchOneStateTime = 0;
			}

			mouseOldStatus = Input.GetMouseButton( 0 );
			mouseOldPosition = Input.mousePosition;
		}
	}

	static bool mouseOldStatus = false;
	static Vector3 mouseOldPosition = Vector3.zero;
	static float mouseOldzoom = 0;

	void updateMouseEmulationTouchState()
	{
		bool isHoveringUI = checkHoveringUI( Input.mousePosition );

		if( Input.GetAxis( "Mouse ScrollWheel" ) != 0 && state == stateTouch.STdefault )
		{
			state = stateTouch.STZoomStart;
		}
		else if( state == stateTouch.STZoomStart )
		{
			state = stateTouch.STZoom;
		}
		else if( state == stateTouch.STZoom )
		{
			if( Input.GetAxis( "Mouse ScrollWheel" ) == 0 )
			{
				state = stateTouch.STZoomEnd;
			}
		}
		else if( state == stateTouch.STZoomEnd )
		{
			state = stateTouch.STdefault;
		}

		Vector3 point1Delta = mouseOldPosition - Input.mousePosition;

		if( Input.GetMouseButton( 0 ) && state == stateTouch.STdefault && !isHoveringUI )
		{
			if( TestDragObjectPosition( Input.mousePosition ) )
			{
				state = stateTouch.STDragStart;
				return;
			}
			else
			{
				if( point1Delta.x > 1 || point1Delta.x < -1 || point1Delta.y > 1 || point1Delta.y < -1 )
				{
					state = stateTouch.STSwipeStart;
					return;
				}
			}
		}
		if( state == stateTouch.STDragStart )
		{
			state = stateTouch.STDrag;
		}
		else if( state == stateTouch.STDrag )
		{
			if( !Input.GetMouseButton( 0 ) )
			{
				state = stateTouch.STDragEnd;
			}
		}
		else if( state == stateTouch.STDragEnd )
		{
			state = stateTouch.STdefault;
		}

		if( state == stateTouch.STSwipeStart )
		{
			state = stateTouch.STSwipe;
		}
		else if( state == stateTouch.STSwipe )
		{
			if( !Input.GetMouseButton( 0 ) )
			{
				state = stateTouch.STSwipeEnd;
			}
		}
		else if( state == stateTouch.STSwipeEnd )
		{
			state = stateTouch.STdefault;
		}
	}

	bool IsPointerOverGameObject0 = false;
	bool IsPointerOverGameObject1 = false;
	void updateTouchArray()
	{
		if( Input.touchCount > 0 )
		{
			if( Input.touches[0].phase == TouchPhase.Began )
			{
				IsPointerOverGameObject0 = checkHoveringUI( Input.touches[0].position );
				touchOneStateMoveTime = 0;
			}
			if( touchOne == null )
			{
				if( !IsPointerOverGameObject0 )
				{
					touchOne = touchClear1;
					touchOne[4] = Input.touches[0];
					touchOne[3] = Input.touches[0];
					touchOne[2] = Input.touches[0];
					touchOne[1] = Input.touches[0];
					touchOne[0] = Input.touches[0];
					touchOneFingerID = Input.touches[0].fingerId;
				}
			}
			else
			{
				if( touchOneFingerID != Input.touches[0].fingerId )
				{
					touchOne = null;
					touchOneFingerID = -1;
					touchOneStateTime = 0;
					return;
				}
				if( touchOne[0].phase != Input.touches[0].phase )
				{
					touchOne[4] = touchOne[3];
					touchOne[3] = touchOne[2];
					touchOne[2] = touchOne[1];
					touchOne[1] = touchOne[0];
					touchOne[0] = Input.touches[0];
					touchOneStateTime = 0;
				}
				else
				{
					if( Input.touches[0].phase == TouchPhase.Moved )
					{
						touchOne[0] = Input.touches[0];

					}
				}

				if( Input.touches[0].phase == TouchPhase.Moved )
				{
					touchOne[0] = Input.touches[0];
					touchOneStateMoveTime += Time.deltaTime;
				}
				touchOneStateTime += Time.deltaTime;
			}
		}
		else
		{
			touchOne = null;
			touchOneFingerID = -1;
			touchOneStateTime = 0;
		}

		if( Input.touchCount > 1 )
		{
			if( Input.touches[1].phase == TouchPhase.Began )
			{
				IsPointerOverGameObject1 = checkHoveringUI( Input.touches[1].position );
			}
			if( touchTwo == null )
			{
				if( !IsPointerOverGameObject1 )
				{
					touchTwo = touchClear2;
					touchTwo[4] = Input.touches[1];
					touchTwo[3] = Input.touches[1];
					touchTwo[2] = Input.touches[1];
					touchTwo[1] = Input.touches[1];
					touchTwo[0] = Input.touches[1];
					touchTwoFingerID = Input.touches[1].fingerId;
				}
			}
			else
			{
				if( touchTwoFingerID != Input.touches[1].fingerId )
				{
					touchTwo = null;
					touchTwoFingerID = -1;
					return;
				}
				if( touchTwo[0].phase != Input.touches[1].phase )
				{
					touchTwo[4] = touchTwo[3];
					touchTwo[3] = touchTwo[2];
					touchTwo[2] = touchTwo[1];
					touchTwo[1] = touchTwo[0];
					touchTwo[0] = Input.touches[1];
				}
				else
				{
					if( Input.touches[1].phase == TouchPhase.Moved )
					{
						touchTwo[0] = Input.touches[1];
					}
				}
			}
		}
		else
		{
			touchTwo = null;
			touchTwoFingerID = -1;
		}
	}

	bool handlerHold()
	{
		if( touchOne != null )
		{
			if( touchOne[0].phase == TouchPhase.Stationary )
			{
				return true;
			}
		}
		return false;
	}

	bool handlerTap()
	{
		if( touchOne != null )
		{
			if( ( touchOne[2].phase == TouchPhase.Began && touchOne[1].phase == TouchPhase.Stationary && touchOne[0].phase == TouchPhase.Ended ) ||
					( touchOne[1].phase == TouchPhase.Began && touchOne[0].phase == TouchPhase.Ended ) ||
					( touchOne[2].phase == TouchPhase.Began && touchOne[1].phase == TouchPhase.Moved && touchOne[0].phase == TouchPhase.Ended && touchOneStateMoveTime < 0.15f ) )
			{
				touchOneStateMoveTime = 10.0f;
				return true;
			}
		}
		return false;
	}

	stateTouch handlerZoom( stateTouch stateT )
	{
		if( stateT == stateTouch.STdefault )
		{
			if( touchTwo != null && touchOne != null )
			{
				/*
				Начало зума:
				Случай 1: (1 M)+(2 M)
				Случай 2: (1 M)+(2 S)
				Случай 3: (1 S)+(2 S)
				Случай 4: (1 S)+(2 M)
				*/
				if( ( touchOne[0].phase == TouchPhase.Moved && touchTwo[0].phase == TouchPhase.Moved ) ||
						( touchOne[0].phase == TouchPhase.Moved && touchTwo[0].phase == TouchPhase.Stationary ) ||
						( touchOne[0].phase == TouchPhase.Stationary && touchTwo[0].phase == TouchPhase.Stationary ) ||
						( touchOne[0].phase == TouchPhase.Stationary && touchTwo[0].phase == TouchPhase.Moved ) )
				{
					return stateTouch.STZoomStart;
				}

			}

		}
		else if( stateT == stateTouch.STZoomStart )
		{
			return stateTouch.STZoom;
		}
		else if( stateT == stateTouch.STZoom )
		{
			/*
			   Зум
			    Случай 1: (1 M)+(2 M)
			    Случай 2: (1 M)+(2 S)
			    Случай 3: (1 S)+(2 M)
			*/
			/*
			    Конец зума:
			    Случай 1: (1 M)+(2 E)
			    Случай 2: (1 S)+(2 E)
			    Случай 3: (1 E)+(2 S)
			    Случай 4: (1 E)+(2 M) */
			if( touchTwo == null || touchOne == null ||
					( ( touchOne[0].phase == TouchPhase.Moved && touchTwo[0].phase == TouchPhase.Ended ) ||
					  ( touchOne[0].phase == TouchPhase.Stationary && touchTwo[0].phase == TouchPhase.Ended ) ||
					  ( touchOne[0].phase == TouchPhase.Ended && touchTwo[0].phase == TouchPhase.Stationary ) ||
					  ( touchOne[0].phase == TouchPhase.Ended && touchTwo[0].phase == TouchPhase.Moved ) ||
					  ( touchOne[0].phase == TouchPhase.Ended && touchTwo[0].phase == TouchPhase.Ended ) ) )
			{
				return stateTouch.STZoomEnd;
			}
		}
		else if( stateT == stateTouch.STZoomEnd )
		{
			return stateTouch.STdefault;
		}
		return stateT;
	}

	stateTouch handlerSwipe( stateTouch stateT )
	{
		if( stateT == stateTouch.STdefault )
		{
			if( touchOne != null )
			{
				/*
				Начало свайпа:
				Случай 1: (1 B)(1 S)(1 M)
				Случай 2: (1 B)(1 M)
				*/

				if( ( /*touchOne[2].phase == TouchPhase.Began&&*/ touchOne[1].phase == TouchPhase.Stationary && touchOne[0].phase == TouchPhase.Moved ) ||
						( /*touchOne[2].phase == TouchPhase.Began &&*/ touchOne[0].phase == TouchPhase.Moved ) )
				{
					return stateTouch.STSwipeStart;
				}
			}
		}
		else if( stateT == stateTouch.STSwipeStart )
		{
			return stateTouch.STSwipe;
		}
		else if( stateT == stateTouch.STSwipe )
		{
			/*
			    Свайп
			    Случай 1: (1 M)
			    Случай 2: (1 S)
			    Конец свайпа:
			    Случай 1: (1 M)(1 E)
			    Случай 1: (1 S)(1 E)
			*/
			if( touchOne == null || touchTwo != null ||
					( touchOne[1].phase == TouchPhase.Stationary && touchOne[0].phase == TouchPhase.Ended ) ||
					( touchOne[1].phase == TouchPhase.Moved && touchOne[0].phase == TouchPhase.Ended ) )
			{
				return stateTouch.STSwipeEnd;
			}
		}
		else if( stateT == stateTouch.STSwipeEnd )
		{
			return stateTouch.STdefault;
		}
		return stateT;
	}

	stateTouch handlerDrag( stateTouch stateT )
	{
		if( stateT == stateTouch.STdefault )
		{
			if( touchOne != null && touchTwo == null )
			{
				/*
				Начало Драга:
				Случай 1: (1 B)(1 S) + S(>0.3C)
				*/
				if( ( /*touchOne[1].phase == TouchPhase.Began &&*/ touchOne[0].phase == TouchPhase.Stationary && TestDragObjectPosition( touchOne[0].position ) /*&& touchOneStateTime > DragTime*/ ) )
				{
					return stateTouch.STDragStart;
				}
			}
		}
		else if( stateT == stateTouch.STDragStart )
		{
			return stateTouch.STDrag;
		}
		else if( stateT == stateTouch.STDrag )
		{
			/*
			    Драг
			    Случай 1: (1 M)
			    Конец Драга:
			    Случай 1: (1 E)
			*/
			if( ( touchOne == null || touchTwo != null ||
					touchOne[0].phase == TouchPhase.Ended ) )
			{
				touchOneStateTime = 0;
				return stateTouch.STDragEnd;
			}
		}
		else if( stateT == stateTouch.STDragEnd )
		{
			return stateTouch.STdefault;
		}
		return stateT;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public bool checkHoveringUI( Vector2 position )
	{
		foreach( RaycastResult r in GetUIRaycastResults( Input.mousePosition ) )
		{
			if( r.gameObject != null )
			{

				if( r.gameObject.CompareTag( "blockForTouch" ) )
				{
					return true;
				}
			}
		}
		return false;
	}

	private static List<RaycastResult> tempRaycastResults = new List<RaycastResult>();
	public List<RaycastResult> GetUIRaycastResults( Vector2 position )
	{
		tempRaycastResults.Clear();
		if( EventSystem.current != null )
		{
			var eventDataCurrentPosition = new PointerEventData( EventSystem.current );
			eventDataCurrentPosition.position = new Vector2( position.x, position.y );
			EventSystem.current.RaycastAll( eventDataCurrentPosition, tempRaycastResults );
		}
		return tempRaycastResults;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool TestDragObjectPosition( Vector3 ScreenPoint )
	{

		GameObject findCamera = getCamera();
		if( findCamera != null )
		{
			Camera cam = findCamera.GetComponent<Camera>();
			if( cam != null )
			{
				Ray ray = cam.ScreenPointToRay( ScreenPoint );
				RaycastHit hit;
				if( Physics.Raycast( ray, out hit, 1000 ) )
				{
				}
				else
				{
				}
			}
		}


		return false;
	}


	public GameObject getCamera()
	{
		GameObject CameraObject = null;
		if( Camera.main != null && Camera.main.gameObject != null )
		{
			CameraObject = Camera.main.gameObject;
		}

		if( CameraObject == null )
		{
			foreach( var camera in GameObject.FindObjectsOfType( typeof( Camera ) ) as Camera[] )
			{
				if( camera != null && camera.gameObject != null )
				{
					CameraObject = camera.gameObject;
					break;
				}
			}
		}
		if( CameraObject == null )
		{
            Debug.LogError( "CameraObject = null" );
		}
		return CameraObject;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////   Zoom                //////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ZoomEvent( stateTouch stateT, Vector3 point1, Vector3 point2, Vector3 point1Delta, Vector3 point2Delta )
	{
		/*
		    Событие зума, имеет 3 состояния Старт,работа,конец
		    Происхотит при работе двумя пальцами, без учета пальцев попавших в элементы UI
		    Дополнительные проверки не нужны
		    point1 - палец 1
		    point2 - палец 2
		    point1Delta - дульта перемешения пальца 1
		    point2Delta - дульта перемешения пальца 2
		*/
#if DEBUG_LOG
		Log.Write( "ZoomEvent: " + state.ToString() + " p1:" + point1.ToString() + " dp1:" + point1Delta.ToString() + " p2:" + point2.ToString() + " dp2:" + point2Delta.ToString() );
#endif
		ZoomModel zoom = new ZoomModel();
		zoom.point1 = point1;
		zoom.point1Delta = point1Delta;
		zoom.point2 = point2;
		zoom.point2Delta = point2Delta;
		zoom.state = stateT;
		dispatcher.Dispatch( Events.E_Zoom, zoom );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////   Swipe                //////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void SwipeEvent( stateTouch stateT, Vector3 point1, Vector3 point1Delta, float EventTime )
	{
		/*
		    Событие протягивания пальца, имеет 3 состояния Старт,работа,конец
		    Происхотит при резком здвижении пальца куда либо, без учета попадания в Drag обьекты, без учета пальцев попавших в элементы UI
		    Дополнительные проверки не нужны
		    point1 - палец 1
		    point1Delta - дульта перемешения пальца 1
		    EventTime - Время нахождения пальца в одном состоянии
		*/
#if DEBUG_LOG
		Log.Write( "SwipeEvent: " + state.ToString() + " p1:" + point1.ToString() + " dp1:" + point1Delta.ToString() );
#endif
		SwipeModel swipe = new SwipeModel();
		swipe.point1 = point1;
		swipe.point1Delta = point1Delta;
		swipe.EventTime = EventTime;
		swipe.state = stateT;
		dispatcher.Dispatch( Events.E_Swipe, swipe );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////   Drag                //////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DragEvent( stateTouch stateT, Vector3 point1, Vector3 point1Delta, float EventTime )
	{
		/*
		    Событие перемешения обьекта, имеет 3 состояния Старт,работа,конец
		    Происхотит при попадании в Drag обьекты, без учета пальцев попавших в элементы UI.
		    При В старте нужно реализовать выдиление здания
		    При В работа перемешение, в конце проверку на отпускания браг обьекта.
		    Дополнительные проверки не нужны
		    point1 - палец 1
		    point1Delta - дульта перемешения пальца 1
		    EventTime - Время нахождения пальца в одном состоянии
		*/
#if DEBUG_LOG
		Log.Write( "DragEvent: " + state.ToString() + " p1:" + point1.ToString() + " dp1:" + point1Delta.ToString() );
#endif
		var dragModel = new DragModel()
		{
			stateT = stateT,
			EventTime = EventTime,
			point1 = point1,
			point1Delta = point1Delta
		};
		if( state == stateTouch.STDragStart )
		{
			dispatcher.Dispatch( Events.E_InputBeginDrag, dragModel );
		}
		else if( state == stateTouch.STDrag )
		{
			dispatcher.Dispatch( Events.E_InputDrag, dragModel );
		}
		else if( state == stateTouch.STDragEnd )
		{
			dispatcher.Dispatch(Events.E_InputEndDrag, dragModel );
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////   Tap                //////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static int tapGlobalCount = 0;
	void TapEvent( stateTouch stateT, Vector3 point1, int tapCount, bool isTapOnDragObject )
	{
		tapGlobalCount++;
		/*
		    Событие тапа по экрану, не имеет состояний срабатывает при отпускании тапа, без учета пальцев попавших в элементы UI.
		    isTapOnDragObject - параметер определяюший произходил ли там над Drag обьектом
		*/
#if DEBUG_LOG
		Log.Write( "TapEvent p1:" + point1.ToString() + " tapCount:" + tapCount.ToString() + " isDragObject:" + isTapOnDragObject.ToString() );
#endif
		var tapModel = new TapModel()
		{
			stateT = stateT,
			point1 = point1,
			tapCount = tapCount,
			isTapOnDragObject = isTapOnDragObject,
			tapGlobalCount = tapGlobalCount
		};
		dispatcher.Dispatch( Events.E_InputTap, tapModel );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void HoldEvent( stateTouch stateT, Vector3 point1, float EventTime, bool isHoldOnDragObject )
	{
		/*
		    Событие Зашатия пальца на экране, не имеет состояний срабатывает зажатии пальца в одном месте, без учета пальцев попавших в элементы UI.
		    isHoldOnDragObject - параметер определяюший произходил ли там над Drag обьектом
		*/
#if DEBUG_LOG
		Log.Write( "HoldEvent p1:" + point1.ToString() + " EventTime:" + EventTime.ToString() + " isDragObject:" + isHoldOnDragObject.ToString() );
#endif
		var tapModel = new HoldModel()
		{
			stateT = stateT,
			point1 = point1,
			EventTime = EventTime,
			isHoldOnDragObject = isHoldOnDragObject
		};
		dispatcher.Dispatch( Events.E_InputHold, tapModel );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
