﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.IO;
using SimpleJSON;
using System.Text;

public enum HTTPMethod
{
    Get,
    Post,
    //todo implement new methods
}

public class NetworkService : INetworkService 
{
    [Inject] public IExecutor Executor { get; set; }

	public const string API_KEY = "087435b47fbc18c51436280dcf69ccad";

    public List<WWW> requests = new List<WWW>();
    public Dictionary<int, Coroutine> coroutines = new Dictionary<int, Coroutine>();

    private Coroutine request;
    int counter = 0;

    public void SendRequest(
        string url,
        Action<WWW> callback,
        Action<float> progressCallback = null,
        HTTPMethod method = HTTPMethod.Get,
        JSONNode data = null,
        Dictionary<string, string> headers = null,
        ThreadPriority priority = ThreadPriority.Normal, 
        bool useAPIKey = true)
    {
        int id = counter++;
        request = Executor.Execute(SendRequestCoroutine(id, url, callback, progressCallback, method, data, headers, priority, useAPIKey));
        coroutines.Add(id, request);
    }

    // HEADERS AND DATA DON'T WORKS
    public IEnumerator SendRequestCoroutine(
        int id,
        string url,
        Action<WWW> callback,
        Action<float> progressCallback = null,
        HTTPMethod method = HTTPMethod.Get,
        JSONNode data = null,
        Dictionary<string, string> headers = null,
        ThreadPriority priority = ThreadPriority.Normal,
        bool useAPIKey = true)
    {
        
        WWW www = null;

        if (data == null)
        {
            data = new JSONClass(); 
        }
        string finalEscapeUriString = "";
        finalEscapeUriString = System.Uri.EscapeUriString(url);
        finalEscapeUriString += BuildUrlEncodedPostData(null, useAPIKey);

        if (headers == null)
        {
            headers = new Dictionary<string, string>();
        }
        Debug.Log("url:: " + finalEscapeUriString);
        www = new WWW(finalEscapeUriString);

        www.threadPriority = priority;
        requests.Add(www);

        while (www != null && string.IsNullOrEmpty(www.error) && !www.isDone)
        {
            if (progressCallback != null)
            {
                progressCallback(www.progress);
            }
            yield return null;
        }
        
        if (www != null)
        {
            callback(www);
            requests.Remove(www);
        }

        coroutines.Remove(id);
    }

    public void AbortAllRequests()
    {
        Debug.LogWarning("WARNING!!!!!!!!!! Aborting all requests, " + requests.Count);
        foreach (var worker in coroutines.Values)
        {
            Executor.StopExecution(worker);
        }
        coroutines.Clear();
        foreach (var request in requests)
        {
            request.Dispose();
        }
        requests.Clear();
    }


    private string BuildUrlEncodedPostData(Dictionary<string, string> data, bool useAPIKey)
    {
        string postData = useAPIKey ? ("?api_token=" + NetworkService.API_KEY) : "";
        if (data == null)
        {
            return postData;
        }
        foreach (var keyValue in data)
        {
            postData += useAPIKey ? "&" : "?";
            postData += string.Format("{0}={1}", EscapeString(keyValue.Key), EscapeString(keyValue.Value));
        }
        Debug.Log(postData);
        return postData;
    }

    private const int EscapeTreshold = 256;

    public string EscapeString(string originalString)
    {
        if (originalString.Length < EscapeTreshold)
            return Uri.EscapeDataString(originalString);
        else
        {
            int loops = originalString.Length / EscapeTreshold;
            StringBuilder sb = new StringBuilder(loops);

            for (int i = 0; i <= loops; i++)
                sb.Append(i < loops ?
                             Uri.EscapeDataString(originalString.Substring(EscapeTreshold * i, EscapeTreshold)) :
                             Uri.EscapeDataString(originalString.Substring(EscapeTreshold * i)));
            return sb.ToString();
        }
    }

}
