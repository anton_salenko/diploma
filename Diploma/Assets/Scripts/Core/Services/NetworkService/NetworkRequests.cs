﻿using UnityEngine;
using System.Collections;


public class NetworkRequests 
{
    public const string APP_ID = "21";
    public const string ServerUrl = "https://cloudliveanimations.org/";
    public const string GetJsonData = "https://cloudliveanimations.org/api/apps/" + APP_ID + ".json";
}
