﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;
using SimpleJSON;

public interface INetworkService
{
    void SendRequest(string url, Action<WWW> callback, Action<float> progressCallback = null, HTTPMethod method = HTTPMethod.Get, JSONNode data = null, Dictionary<string, string> headers = null, ThreadPriority priority = ThreadPriority.Normal, bool useAPIKey = true);

    void AbortAllRequests();
}
