﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICollectionsMediator : BaseMediator {

    [Inject] public UICollectionsView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
