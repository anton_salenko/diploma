﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using System.IO;

public class UICollectionsView : BaseView {

    [SerializeField] Button backButton;
    [SerializeField] Animator animator;
    [SerializeField] string closeTrigger;
    [SerializeField] float showTime = 1f;
    [SerializeField] float closeTime = 1f;
    [SerializeField] UICollectionView collectionTemplate; 
    [SerializeField] Transform collectionContainer;

    [Inject] public SFXManager sfxManager { get; set; }
    [Inject] public AppDataModel appData { get; set; }


    public void LoadView()
    {
        backButton.onClick.AddListener(OnBackClick);
        AnimateShow();
        InstantiateCollections();
    }

    public void RemoveView()
    {

    }

    private void InstantiateCollections()
    {
        foreach (var book in appData.Books.Values)
        {
            UICollectionView collection = Instantiate(collectionTemplate, collectionContainer);
            collection.gameObject.SetActive(true);
            collection.Init(book);
        }
    }

    private void OnBackClick()
    {
        StartCoroutine(AnimateClosing());
    }

    private IEnumerator AnimateClosing()
    {
        dispatcher.Dispatch(Events.DisableBlur);
        animator.SetTrigger(closeTrigger);
        yield return new WaitForSeconds(closeTime);
        dispatcher.Dispatch(Events.UICollectionsRemove);
    }

    private IEnumerator AnimateShow()
    {
        yield return new WaitForSeconds(showTime);
    }

}
