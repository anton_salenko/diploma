﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICollectionMediator : BaseMediator {

    [Inject] public UICollectionView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
