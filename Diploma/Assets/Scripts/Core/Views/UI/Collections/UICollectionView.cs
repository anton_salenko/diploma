﻿using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICollectionView : BaseView {

    [SerializeField] Text nameText;
    [SerializeField] Image image;
    [SerializeField] Button button;
    [SerializeField] Slider progressBar;
    [SerializeField] Image successImage;
    [SerializeField] Image failImage;
    private float progress = 0f;

    private CollectionModel model;

    bool isLoading = false;


    private TextureRequestModel textureRequest = null;

    public void LoadView()
    {
        button.onClick.AddListener(OnClick);
        progressBar.gameObject.SetActive(false);
    }

    public void RemoveView()
    {
        if (dispatcher.HasListener(Events.E_LoadingRemoteARContentFail, OnLoadFail))
        {
            dispatcher.RemoveListener(Events.E_LoadingRemoteARContentFail, OnLoadFail);
        }
        if (dispatcher.HasListener(Events.E_LoadingRemoteARContentSuccess, OnLoadSuccess))
        {
            dispatcher.RemoveListener(Events.E_LoadingRemoteARContentSuccess, OnLoadSuccess);
        }

    }

    private void OnClick()
    {
        if (model.CheckAllAnimationsPresent())
        {
            return;
        }
        isLoading = true;
        progressBar.gameObject.SetActive(true);
        dispatcher.Dispatch(Events.E_LoadRemoteARContent, model);
        dispatcher.AddListener(Events.E_LoadingRemoteARContentFail, OnLoadFail);
        dispatcher.AddListener(Events.E_LoadingRemoteARContentSuccess, OnLoadSuccess);
    }

    private void Update()
    {
        if (model != null && isLoading)
        {
            progressBar.value = model.loadingProgress;
        }
    }

    private void OnLoadFail(IEvent eventData)
    {
        if (eventData.data != model)
        {
            return;
        }
        isLoading = false;
        progressBar.gameObject.SetActive(false);
        progressBar.value = 0f;
        failImage.gameObject.SetActive(true);
        successImage.gameObject.SetActive(false);
    }

    private void OnLoadSuccess(IEvent eventData)
    {
        if (eventData.data != model)
        {
            return;
        }
        isLoading = false;
        progressBar.gameObject.SetActive(false);
        progressBar.value = 1f;
        failImage.gameObject.SetActive(false);
        successImage.gameObject.SetActive(true);
    }

    public void Init(CollectionModel model)
    {
        this.model = model;
        nameText.text = model.Name;
        LoadSprite(model.ImagePath);
        successImage.gameObject.SetActive(model.CheckAllAnimationsPresent());
    }

    private void LoadSprite(string path)
    {
        string url = GetFileURL(path);

        if (textureRequest != null)
        {
            textureRequest.callback = null;
        }

        textureRequest = new TextureRequestModel(url, (texture) =>
        {
            image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100);
            image.color = Color.white;
            Resources.UnloadUnusedAssets();
        });

        dispatcher.Dispatch(Events.E_DownloadTexture, textureRequest);
    }
    

    public string GetFileURL(string path)
    {
        if (Application.isEditor)
        {
            return "file:///" + path;
        }
        else
        {
            return "file://" + path;
        }
    }
}
