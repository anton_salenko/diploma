﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMapMediator : BaseMediator
{

    [Inject] public UIMapView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
