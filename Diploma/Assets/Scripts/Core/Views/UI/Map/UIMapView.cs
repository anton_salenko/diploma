﻿using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMapView : BaseView
{

    [SerializeField] Button backButton;
    [SerializeField] Animator animator;
    [SerializeField] string closeTrigger;
    [SerializeField] float showTime = 1f;
    [SerializeField] float closeTime = 1f;
    [SerializeField] RectTransform mapTransform;
    [SerializeField] RectTransform mapHolder;
    [SerializeField] Image mapImage;
    [SerializeField] MapPointBehaviour mapPointTemplate;

    [Inject] public SFXManager sfxManager { get; set; }
    [Inject] public AppDataModel appData { get; set; }

    private Vector2 swipeStartPos = Vector3.zero;
    private Vector3 zoomStartPos1 = Vector3.zero;
    private Vector3 zoomStartPos2 = Vector3.zero;
    private float zoomStartDistance = 0f;
    private float zoomStartScale = 1f;

    private Vector2 startAnchorPos = Vector2.zero;

    private TextureRequestModel textureRequest = null;
    private CollectionModel collection;

    private Dictionary<ARContentModel, MapPointBehaviour> points = new Dictionary<ARContentModel, MapPointBehaviour>();

    public void InitCollection(CollectionModel model)
    {
        collection = model;
        LoadSprite(model.MapPath);
        InitPoints();
    }

    private void InitPoints()
    {
        foreach (var arContent in collection.Animations)
        {
            var point = Instantiate(mapPointTemplate, mapImage.transform);
            point.SetPosition(arContent.MapPosition);
            point.SetActive(arContent.HasPlayed);
            point.SetLabel(arContent.MapLable);
            point.gameObject.SetActive(true);
        }
    }

    public void LoadView()
    {
        dispatcher.AddListener(Events.E_Zoom, OnZoom);
        dispatcher.AddListener(Events.E_Swipe, OnSwipe);
        backButton.onClick.AddListener(OnBackClick);
        startAnchorPos = mapTransform.anchoredPosition;
        AnimateShow();
    }

    public void RemoveView()
    {
        dispatcher.RemoveListener(Events.E_Zoom, OnZoom);
        dispatcher.RemoveListener(Events.E_Swipe, OnSwipe);
    }

    private void OnBackClick()
    {
        StartCoroutine(AnimateClosing());
    }

    private IEnumerator AnimateClosing()
    {
        dispatcher.Dispatch(Events.DisableBlur);
        animator.SetTrigger(closeTrigger);
        yield return new WaitForSeconds(closeTime);
        dispatcher.Dispatch(Events.UIMapHide);
    }

    private IEnumerator AnimateShow()
    {
        yield return new WaitForSeconds(showTime);
    }
    
    private void OnZoom(IEvent eventData)
    {
        var zoom = eventData.data as ZoomModel;
        Vector2 fingerPos;
        if (zoom.state == MainContextInput.stateTouch.STZoomStart)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(mapHolder, (zoom.point1 + zoom.point2) / 2f, null, out fingerPos);
            swipeStartPos = fingerPos;
            startAnchorPos = mapTransform.anchoredPosition;
            zoomStartDistance = Vector3.Distance(zoom.point1, zoom.point2);
            zoomStartScale = mapTransform.localScale.x;
            return;
        }

        Vector2 zoomedStartPos = swipeStartPos * (Vector3.Distance(zoom.point1, zoom.point2) / zoomStartDistance);
        mapTransform.localScale = Vector3.one * Mathf.Clamp(zoomStartScale * (Vector3.Distance(zoom.point1, zoom.point2) / zoomStartDistance), 0.5f, 2f);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(mapHolder, (zoom.point1 + zoom.point2) / 2f, null, out fingerPos);
        var deltaPos = fingerPos - zoomedStartPos;
        mapTransform.anchoredPosition = startAnchorPos + (Vector2)deltaPos;
        var size = mapTransform.sizeDelta / 2f;
        mapTransform.anchoredPosition = new Vector2(Mathf.Clamp(mapTransform.anchoredPosition.x, -size.x, size.x), Mathf.Clamp(mapTransform.anchoredPosition.y, -size.y, size.y));


    }

    private void OnSwipe(IEvent eventData)
    {
        Vector2 fingerPos;
        var swipe = eventData.data as SwipeModel;
        if (swipe.state == MainContextInput.stateTouch.STSwipeStart)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(mapHolder, swipe.point1, null, out fingerPos);
            swipeStartPos = fingerPos;
            startAnchorPos = mapTransform.anchoredPosition;
            return;
        }

        RectTransformUtility.ScreenPointToLocalPointInRectangle(mapHolder, swipe.point1, null, out fingerPos);
        var deltaPos = fingerPos - swipeStartPos;
        mapTransform.anchoredPosition = startAnchorPos + (Vector2)deltaPos;
        var size = mapTransform.sizeDelta / 2f;
        mapTransform.anchoredPosition = new Vector2(Mathf.Clamp(mapTransform.anchoredPosition.x, -size.x, size.x), Mathf.Clamp(mapTransform.anchoredPosition.y, -size.y, size.y));


    }


    private void LoadSprite(string path)
    {
        string url = GetFileURL(path);

        if (textureRequest != null)
        {
            textureRequest.callback = null;
        }

        textureRequest = new TextureRequestModel(url, (texture) =>
        {
            mapImage.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100);
            mapImage.color = Color.white;
            Resources.UnloadUnusedAssets();
        });

        dispatcher.Dispatch(Events.E_DownloadTexture, textureRequest);
    }


    public string GetFileURL(string path)
    {
        if (Application.isEditor)
        {
            return "file:///" + path;
        }
        else
        {
            return "file://" + path;
        }
    }
}
