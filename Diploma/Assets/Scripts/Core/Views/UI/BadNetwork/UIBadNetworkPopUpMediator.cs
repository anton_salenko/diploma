﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBadNetworkPopUpMediator : BaseMediator {

    [Inject]
    public UIBadNetworkPopUpView view { get; set; }

    public override void PreRegister()
    {

    }
    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
