﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBadNetworkPopUpView : BaseView {

    [SerializeField] Button closeButton;
    [SerializeField] Animator animator;
    [SerializeField] string closeTrigger;
    [SerializeField] float showTime = 1f;
    [SerializeField] float closeTime = 1f;

    public void LoadView()
    {
        closeButton.onClick.AddListener(OnBack);
        AnimateShow();
    }
	
    public void RemoveView()
    {

    }

    private void OnBack()
    {
        StartCoroutine(AnimateClosing());

    }

    private IEnumerator AnimateClosing()
    {
        animator.SetTrigger(closeTrigger);
        yield return new WaitForSeconds(closeTime);
        dispatcher.Dispatch(Events.E_RetryDownloading);
        dispatcher.Dispatch(Events.E_BadNetworkPopUpHide);
    }

    private IEnumerator AnimateShow()
    {
        yield return new WaitForSeconds(showTime);
    }
}
