﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISettingsView : BaseView
{
    [SerializeField] Toggle musicOnOff;
    [SerializeField] Toggle systemSoundsOnOff;
    [SerializeField] Button clearCacheButton;
    [SerializeField] Button backButton;
    [SerializeField] Animator animator;
    [SerializeField] string closeTrigger;
    [SerializeField] float showTime = 1f;
    [SerializeField] float closeTime = 1f;

    [Inject] public SFXManager sfxManager { get; set; }


    public void LoadView()
    {
        musicOnOff.isOn = sfxManager.MusicVolume == 1;
        systemSoundsOnOff.isOn = sfxManager.SoundVolume == 1;
        musicOnOff.onValueChanged.AddListener(OnMusicToggleClick);
        systemSoundsOnOff.onValueChanged.AddListener(OnSoundToggleClick);

        backButton.onClick.AddListener(OnBackClick);
        clearCacheButton.onClick.AddListener(OnClearCacheClick);

        AnimateShow();
    }

    public void RemoveView()
    {

    }

    private void OnMusicToggleClick(bool value)
    {
        sfxManager.MusicVolume = value ? 1 : 0;
        dispatcher.Dispatch(Events.SFXOnVolumeChanged);
    }

    private void OnSoundToggleClick(bool value)
    {
        sfxManager.SoundVolume = value ? 1 : 0;
        dispatcher.Dispatch(Events.SFXOnVolumeChanged);
    }

    private void OnBackClick()
    {
        StartCoroutine(AnimateClosing());
    }

    private void OnClearCacheClick()
    {

    }

    private IEnumerator AnimateClosing()
    {
        dispatcher.Dispatch(Events.DisableBlur);
        animator.SetTrigger(closeTrigger);
        yield return new WaitForSeconds(closeTime);
        dispatcher.Dispatch(Events.UISettingsRemove);
    }

    private IEnumerator AnimateShow()
    {
        yield return new WaitForSeconds(showTime);
    }

}
