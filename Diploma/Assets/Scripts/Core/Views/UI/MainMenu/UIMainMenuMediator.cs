﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainMenuMediator : BaseMediator {

    [Inject] public UIMainMenuView view { get; set; }

    public override void OnRegister()
    {
        Debug.Log("sdgbsrfgbadgb");
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
