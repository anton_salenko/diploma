﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenuView : BaseView {


    [SerializeField] Button cameraButton;
    [SerializeField] Button collectionsButton;
    [SerializeField] Button settingsButton;

	public void LoadView()
    {
        cameraButton.onClick.AddListener(OnCameraClick);
        collectionsButton.onClick.AddListener(OnCollectionsClick);
        settingsButton.onClick.AddListener(OnSettingsClick);
    }

    public void RemoveView()
    {

    }

    private void OnCameraClick()
    {
        dispatcher.Dispatch(Events.UIMainMenuRemove);
        dispatcher.Dispatch(Events.UIHUDAdd);
        dispatcher.Dispatch(Events.VuforiaARCameraEnable);
        dispatcher.Dispatch(Events.E_AddAllMarkers);
    }

    private void OnSettingsClick()
    {
        dispatcher.Dispatch(Events.UISettingsAdd);
        dispatcher.Dispatch(Events.EnableBlur);
    }

    public void OnCollectionsClick()
    {
        dispatcher.Dispatch(Events.UICollectionsAdd);
        dispatcher.Dispatch(Events.EnableBlur);
    }

}
