﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;

public class UILoadingView : BaseView {

    [SerializeField] Slider loadingSlider;
    [SerializeField] Text loadingText;
    [SerializeField] string loadingTextFormat = "LOADING {0}%";

    public AnimationCurve loadingAnim;
    public float totalAnimTime = 2f;
    float timeElapsed = 0f;
    float progress = 0f;

    public void LoadView()
    {
        dispatcher.AddListener(Events.E_UpdateBooksDataSuccess, OnSuccessLoading);
        dispatcher.AddListener(Events.E_StartUsingLocalConfigs, OnSuccessLoading);

        dispatcher.AddListener(Events.E_UpdateBooksDataFail, OnFailLoading);
        dispatcher.AddListener(Events.E_UpdateBooksDataProgress, OnLoadingProgress);
    }

    private void OnSuccessLoading()
    {
        dispatcher.Dispatch(Events.UIMainMenuAdd);
        dispatcher.Dispatch(Events.UILoadingRemove);
    }

    private void OnFailLoading()
    {
        Debug.LogError("fail");
    }

    private void OnLoadingProgress(IEvent eventData)
    {
        progress = (float)eventData.data;

    }

    void Update()
    {
        if (timeElapsed < totalAnimTime)
        {
            timeElapsed += Time.deltaTime;
        }
        loadingSlider.value = (progress + timeElapsed / totalAnimTime) / 2f;
        loadingText.text = string.Format(loadingTextFormat, Mathf.CeilToInt(progress * 100));

    }

    public void RemoveView()
    {
        dispatcher.RemoveListener(Events.E_StartUsingLocalConfigs, OnSuccessLoading);

        dispatcher.RemoveListener(Events.E_UpdateBooksDataSuccess, OnSuccessLoading);
        dispatcher.RemoveListener(Events.E_UpdateBooksDataFail, OnFailLoading);
        dispatcher.RemoveListener(Events.E_UpdateBooksDataProgress, OnLoadingProgress);
    }
}
