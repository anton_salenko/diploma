﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoadingMediator : BaseMediator {

    [Inject] public UILoadingView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
