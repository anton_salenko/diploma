﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using System.IO;

public class UIHUDView : BaseView {

    [SerializeField] Button mapButton;
    [SerializeField] Button pauseButton;
    [SerializeField] Button photoButton;
    [SerializeField] Button backButton;
    [SerializeField] Button closeButton;

	public void LoadView()
    {
        mapButton.onClick.AddListener(OnMapClick);
        pauseButton.onClick.AddListener(OnPauseClick);
        photoButton.onClick.AddListener(OnPhotoClick);
        backButton.onClick.AddListener(OnBackClick);
        closeButton.onClick.AddListener(OnCloseClick);
    }

    public void RemoveView()
    {

    }

    private void OnMapClick()
    {
        dispatcher.Dispatch(Events.UIMapShow);
        dispatcher.Dispatch(Events.EnableBlur);
    }

    private void OnPauseClick()
    {

    }

    private void OnPhotoClick()
    {
#if UNITY_EDITOR
        Debug.Log("capturing not available");
#else
        StartCoroutine(ShareCoroutine());
#endif
    }

    private IEnumerator ShareCoroutine()
    {
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        string filePath = Path.Combine(Application.temporaryCachePath, "shared_img.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());

        Destroy(ss);

        new NativeShare().AddFile(filePath).SetText("Photo").Share();
    }

    private string GenerateScreenshotFilename()
    {
        return string.Format("photo_{0}.png", System.DateTime.Now.ToString("yyyy_MM_dd_hh-mm-ss"));
    }

    private void OnBackClick()
    {
        dispatcher.Dispatch(Events.VuforiaARCameraDisable);
        dispatcher.Dispatch(Events.UIHUDRemove);
        dispatcher.Dispatch(Events.UIMainMenuAdd);
    }

    private void OnCloseClick()
    {
        dispatcher.Dispatch(Events.ClearARContent);
    }
}
