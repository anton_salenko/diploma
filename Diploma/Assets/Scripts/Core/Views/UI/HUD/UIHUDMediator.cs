﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHUDMediator : BaseMediator {

    [Inject] public UIHUDView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
