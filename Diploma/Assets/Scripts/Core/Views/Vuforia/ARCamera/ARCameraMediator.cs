﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARCameraMediator : BaseMediator
{

    [Inject] public ARCameraView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
