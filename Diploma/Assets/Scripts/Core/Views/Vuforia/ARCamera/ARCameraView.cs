﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARCameraView : BaseView {

    private bool onMarker = false;

    private BlurPostEffect blur;

    private Coroutine blurCoroutine = null;

    public void LoadView()
    {
        dispatcher.AddListener(Events.E_VFTrackableARMarkerTargetsFound, OnMarkerFounded);
        dispatcher.AddListener(Events.E_VFTrackableARMarkerTargetsLost, OnMarkerLost);
        dispatcher.AddListener(Events.EnableBlur, OnBlurEnable);
        dispatcher.AddListener(Events.DisableBlur, OnBlurDisable);
        blur = GetComponent<BlurPostEffect>();
        Debug.Log("view loaded");
    }

    public void RemoveView()
    {
        dispatcher.RemoveListener(Events.E_VFTrackableARMarkerTargetsFound, OnMarkerFounded);
        dispatcher.RemoveListener(Events.E_VFTrackableARMarkerTargetsLost, OnMarkerLost);
        dispatcher.RemoveListener(Events.EnableBlur, OnBlurEnable);
        dispatcher.RemoveListener(Events.DisableBlur, OnBlurDisable);
        Debug.Log("view removed");
    }

    private void OnBlurEnable()
    {
        if (blurCoroutine != null)
        {
            StopCoroutine(blurCoroutine);
        }
        blurCoroutine = StartCoroutine(BlurEnableCoroutine());
    }

    private IEnumerator BlurEnableCoroutine()
    {
        float time = Time.time;
        blur.enabled = true;
        while (time + 1f > Time.time)
        {
            blur.power = Mathf.Lerp(1f, 32f, Time.time - time);
            yield return null;
        }
    }

    private void OnBlurDisable()
    {
        if (blurCoroutine != null)
        {
            StopCoroutine(blurCoroutine);
        }
        blurCoroutine = StartCoroutine(BlurDisableCoroutine());
    }

    private IEnumerator BlurDisableCoroutine()
    {
        float time = Time.time;
        while (time + 1f > Time.time)
        {
            blur.power = Mathf.Lerp(32f, 1f, Time.time - time);
            yield return null;
        }
        blur.enabled = false;
    }

    private void OnMarkerFounded()
    {
        onMarker = true;
    }

    private void OnMarkerLost()
    {
        onMarker = false;
    }

    private void Update()
    {
        if (!onMarker)
        {
            transform.localRotation = Quaternion.identity;
            transform.position = Vector3.zero;
        }
    }

}
