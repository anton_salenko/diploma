﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerTargetsMediator : BaseMediator
{

    [Inject] public MarkerTargetsView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }

}
