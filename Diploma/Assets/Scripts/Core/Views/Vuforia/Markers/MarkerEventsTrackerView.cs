﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MarkerEventsTrackerView : BaseView, ITrackableEventHandler
{
    #region PROTECTED_MEMBER_VARIABLES

    public int ImageTargetID { get; set; }

    protected TrackableBehaviour mTrackableBehaviour;

    #endregion // PROTECTED_MEMBER_VARIABLES

    #region UNITY_MONOBEHAVIOUR_METHODS

    public void LoadView()
    {

    }

    public void RemoveView()
    {

    }

    protected override void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    protected override void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    #endregion // UNITY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                    newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PROTECTED_METHODS

    protected virtual void OnTrackingFound()
    {

        var canvasComponents = GetComponentsInChildren<Canvas>(true);


        // Enable canvas':
        foreach (var component in canvasComponents)
            component.enabled = true;

        dispatcher.Dispatch(Events.MarkerFounded, mTrackableBehaviour);
    }


    protected virtual void OnTrackingLost()
    {

        var canvasComponents = GetComponentsInChildren<Canvas>(true);


        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;

        dispatcher.Dispatch(Events.MarkerLost, mTrackableBehaviour);

    }

    #endregion // PROTECTED_METHODS
}