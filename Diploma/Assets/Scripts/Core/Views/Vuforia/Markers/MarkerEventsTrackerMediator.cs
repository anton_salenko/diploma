﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerEventsTrackerMediator : BaseMediator
{
    [Inject] public MarkerEventsTrackerView view { get; set; }

    public override void OnRegister()
    {
        view.LoadView();
    }

    public override void OnRemove()
    {
        view.RemoveView();
    }
}
