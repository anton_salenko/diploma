﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARContentView : BaseView {

    [SerializeField] float magnetDepthLength = 2.7f;
    private bool onMarker = false;

    public void LoadView()
    {
        dispatcher.AddListener(Events.E_VFTrackableARMarkerTargetsFound, OnMarkerFounded);
        dispatcher.AddListener(Events.E_VFTrackableARMarkerTargetsLost, OnMarkerLost);
        Input.gyro.enabled = true;
    }

    public void RemoveView()
    {
        dispatcher.RemoveListener(Events.E_VFTrackableARMarkerTargetsFound, OnMarkerFounded);
        dispatcher.RemoveListener(Events.E_VFTrackableARMarkerTargetsLost, OnMarkerLost);
    }

    private void OnMarkerFounded()
    {
        onMarker = true;
    }

    private void OnMarkerLost()
    {
        onMarker = false;
    }

    private void Update()
    {
        if (onMarker)
        {
            UpdateContentOnMarker();
        }
        else
        {
            UpdateContentOnMagnet();
        }
    }

    private void UpdateContentOnMarker()
    {
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
    }

    private void UpdateContentOnMagnet()
    {
        var targetTransform = Camera.main.transform;
        transform.position = targetTransform.position + targetTransform.forward * magnetDepthLength;

        if (SystemInfo.supportsGyroscope)
        {
            Debug.Log("magnet");
            var gyroRotation = Input.gyro.attitude;

            transform.LookAt(targetTransform);
            var rotation = Quaternion.Euler(-90f, 0f, 0f) * (gyroRotation * Quaternion.Euler(0f, 0f, 0f));
            transform.localRotation = Quaternion.Euler(0f, 180f, 0f) * Quaternion.Euler(0f, 0f, rotation.eulerAngles.z) * Quaternion.Euler(-rotation.eulerAngles.x, 0f, 0f) * transform.localRotation;
        }
        else
        {
            transform.LookAt(targetTransform, targetTransform.up);
            transform.localRotation *= Quaternion.Euler(-35f, 180f, 0f);
        }
    }
}
