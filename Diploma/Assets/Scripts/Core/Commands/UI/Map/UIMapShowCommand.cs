﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMapShowCommand : BaseCommand
{

    private const string NAME = "UIMap";
    private const string PATH = "Prefabs/UI/UIMap";

    [Inject] public AppDataModel appData { get; set; }

    public override void Execute()
    {
        if (appData.SelectedCollection == null)
        {
            return;
        }
        var prefab = Resources.Load<GameObject>(PATH);
        if (prefab == null)
        {
            Debug.LogError("prefab is null at " + PATH);
            Fail();
        }

        var GO = GameObject.Instantiate<GameObject>(prefab);
        GO.name = NAME;

        var view = GO.GetComponent<UIMapView>();
        view.InitCollection(appData.SelectedCollection);
        if (view == null)
        {
            Debug.LogError("GO has no view at " + PATH);
            Fail();
        }

    }

}
