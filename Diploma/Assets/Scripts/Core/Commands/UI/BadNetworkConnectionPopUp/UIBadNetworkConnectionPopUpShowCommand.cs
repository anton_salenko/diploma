﻿using strange.extensions.mediation.api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBadNetworkConnectionPopUpShowCommand : BaseCommand {

    public const string PATH = "Prefabs/UI/UIBadNetworkPopUp";
    public const string NAME = "UIBadNetworkPopUp";

    public override void Execute()
    {

        var prefab = Resources.Load<GameObject>(PATH);
        if (prefab == null)
        {
            Debug.LogError("Loading error: " + PATH);
            Fail();
            return;
        }

        var GO = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
        GO.name = NAME;

        Canvas canvas = GO.GetComponent<Canvas>();
        if (canvas != null)
        {
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        }
        else
        {
            Debug.LogError("Can't find camera or canvas in " + NAME);
        }

        var view = GO.GetComponent<BaseView>();
    }

}
