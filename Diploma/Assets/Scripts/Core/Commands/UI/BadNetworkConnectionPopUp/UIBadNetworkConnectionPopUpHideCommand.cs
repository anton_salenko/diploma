﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBadNetworkConnectionPopUpHideCommand : BaseCommand {

    public const string NAME = "UIBadNetworkPopUp";

    public override void Execute()
    {
        var GO = GameObject.Find(NAME);
        if (GO == null)
        {
            Debug.LogWarning("Can't find " + NAME);
            Fail();
            return;
        }
        GameObject.Destroy(GO);
        GO = null;
        Resources.UnloadUnusedAssets();
    }

}
