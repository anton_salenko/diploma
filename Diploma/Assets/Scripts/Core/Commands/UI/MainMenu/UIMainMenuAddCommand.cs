﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainMenuAddCommand : BaseCommand {

    private const string NAME = "UIMainMenu";
    private const string PATH = "Prefabs/UI/UIMainMenu";

    public override void Execute()
    {
        var prefab = Resources.Load<GameObject>(PATH);
        if (prefab == null)
        {
            Debug.LogError("prefab is null at " + PATH);
            Fail();
        }

        var GO = GameObject.Instantiate<GameObject>(prefab);
        GO.name = NAME;

        var canvas = GO.GetComponent<Canvas>();

        if (canvas != null)
        {
            canvas.worldCamera = Camera.main;
        }

        var view = GO.GetComponent<UIMainMenuView>();

        if (view == null)
        {
            Debug.LogError("GO has no view at " + PATH);
            Fail();
        }

    }
}
