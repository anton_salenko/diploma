﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISettingsAddCommand : BaseCommand
{
    private const string NAME = "UISettings";
    private const string PATH = "Prefabs/UI/UISettings";

    public override void Execute()
    {
        Debug.Log("asfadf");
        var prefab = Resources.Load<GameObject>(PATH);
        if (prefab == null)
        {
            Debug.LogError("prefab is null at " + PATH);
            Fail();
        }

        var GO = GameObject.Instantiate<GameObject>(prefab);
        GO.name = NAME;

        var view = GO.GetComponent<UISettingsView>();

        if (view == null)
        {
            Debug.LogError("GO has no view at " + PATH);
            Fail();
        }

    }
}
