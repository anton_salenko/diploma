﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoadingViewRemoveCommand : BaseCommand {

    private const string VIEW_NAME = "UILoading";
    public override void Execute()
    {
        var view = GameObject.Find(VIEW_NAME);

        if (view == null)
        {
            Fail();
            return;
        }

        GameObject.Destroy(view);
        view = null;
    }
}
