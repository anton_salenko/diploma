﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Text;  // for class Encoding
using System.IO;    // for StreamReader
using System;

public class NetworkPostEmailCommand : BaseCommand
{

    private bool isSuccess;

    public override void Execute()
    {

        var model = eventData.data as EmailPostRequestModel;

        Retain();
        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        isSuccess = false;

        try
        {
            var request = (HttpWebRequest)WebRequest.Create(model.Url);

            var postData = BuildUrlEncodedPostData(model.PostData);
            var bytesData = Encoding.UTF8.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytesData.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(bytesData, 0, bytesData.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            Debug.Log("Email Sended (response from server): " + responseString);
            isSuccess = true;
        }
        catch (WebException ex)
        {
            Debug.LogError("WebException Message: " + ex.Message);
            Debug.LogError("WebException Status: " + ex.Status);
            isSuccess = false;
        }
        catch
        {
            isSuccess = false;
        }
        finally
        {
            model.OnResponse(isSuccess);
        }
    }

    private string BuildUrlEncodedPostData(Dictionary<string, string> data)
    {
        string postData = "api_token=" + NetworkService.API_KEY;
        foreach (var keyValue in data)
        {
            postData += "&";
            postData += string.Format("{0}={1}", EscapeString(keyValue.Key), EscapeString(keyValue.Value));
        }
        Debug.Log(postData);
        return postData;
    }

    private const int EscapeTreshold = 256;

    public string EscapeString(string originalString)
    {
        if (originalString.Length < EscapeTreshold)
            return Uri.EscapeDataString(originalString);
        else
        {
            int loops = originalString.Length / EscapeTreshold;
            StringBuilder sb = new StringBuilder(loops);

            for (int i = 0; i <= loops; i++)
                sb.Append(i < loops ?
                             Uri.EscapeDataString(originalString.Substring(EscapeTreshold * i, EscapeTreshold)) :
                             Uri.EscapeDataString(originalString.Substring(EscapeTreshold * i)));
            return sb.ToString();
        }
    }

}
