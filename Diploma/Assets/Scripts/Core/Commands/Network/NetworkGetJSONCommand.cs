﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class NetworkGetJSONCommand : BaseCommand
{

    [Inject]
    public INetworkService networkService { get; set; }

    private JSONRequestModel request;

    public override void Execute()
    {
        request = eventData.data as JSONRequestModel;
        if (request == null)
        {
            Debug.LogError("NetworkGetJSONCommand: eventData.data != JSONRequestModel");
            Fail();
            return;
        }

        Retain();

        networkService.SendRequest(request.URL, OnResponse, null, HTTPMethod.Get, request.Data, request.Headers);

    }

    private void OnResponse(WWW www)
    {
        if (!string.IsNullOrEmpty(www.error))
        {
            request.Error(www);
            //request.Callback();
            Release();
            Fail();
            return;
        }

        string json = www.text;
        request.SetJSON(json);
        request.Callback();
        Release();


    }
}
