﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkLoadFileCommand : BaseCommand {

    [Inject]
    public INetworkService networkService { get; set; }

    private FileRequestModel request;
    private int errorCounter = 5;

    private float startRequestTime;


    public override void Execute()
    {
        request = eventData.data as FileRequestModel;
        if (request == null)
        {
            Debug.LogError("NetworkLoadFileCommand: eventData.data != FileRequestModel");
            Fail();
            return;
        }

        Retain();
        startRequestTime = Time.time;
        SendRequest();
        
    }

    private void SendRequest()
    {
        networkService.SendRequest(request.URL, OnResponse, ShowProgress, HTTPMethod.Get, request.Data, request.Headers, priority: request.priority);
    }

    private void ShowProgress(float progress)
    {
        request.ProgressCallback(progress);
    }


    private void OnResponse(WWW www)
    {
        request.www = www;
        if (!string.IsNullOrEmpty(www.error))
        {
            if (--errorCounter <= 0)
            {
                if (!string.IsNullOrEmpty(www.text))
                {
                    var json = SimpleJSON.JSON.Parse(www.text);
                    if (json["status"].AsInt == 500)
                    {
                        Debug.LogWarning(www.text);
                        request.Callback();
                        Release();
                        return;
                    }

                }
                request.Error(www);
                //request.Callback();
                Release();
                Fail();
                return;
            }
            else
            {
                //Debug.LogWarning(www.error);
                SendRequest();
            }
        }
        else
        {
            //Debug.Log("request time: " + (Time.time - startRequestTime) + "\nrequest size: " + www.bytes.Length / 1024f + " kb");
            request.SetFile(www.bytes);

            request.Callback();
            www.Dispose();
            Release();
        }
    }
}
