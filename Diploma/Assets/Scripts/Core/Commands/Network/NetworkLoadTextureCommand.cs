﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkLoadTextureCommand : BaseCommand
{

    [Inject]
    public INetworkService networkService { get; set; }

    private TextureRequestModel request;

    public override void Execute()
    {
        request = eventData.data as TextureRequestModel;
        if (request == null)
        {
            Debug.LogError("NetworkGetJSONCommand: eventData.data != JSONRequestModel");
            Fail();
            return;
        }

        Retain();

        networkService.SendRequest(request.URL, OnResponse, null, HTTPMethod.Get, request.Data, request.Headers, ThreadPriority.High, false);

    }

    private void OnResponse(WWW www)
    {
        if (!string.IsNullOrEmpty(www.error))
        {
            request.Error(www);
            Release();
            Fail();
            return;
        }

        request.SetTexture(www.texture);
        request.Callback();
        Release();


    }
}
