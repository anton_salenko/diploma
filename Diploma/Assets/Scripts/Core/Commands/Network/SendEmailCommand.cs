﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

public class SendEmailCommand : BaseCommand
{
    public override void Execute()
    {
        SendEmailModel model = (SendEmailModel)eventData.data;
        executor.Execute(SendWWW(model));
        //Send(model);
    }

    public bool RemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        // If there are errors in the certificate chain, look at each error to determine the cause.
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                    }
                }
            }
        }
        return isOk;
    }

    private void Send(SendEmailModel model)
    {
        bool isSuccess = false;
        System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls12;
        System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11;
        System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls;
        ServicePointManager.ServerCertificateValidationCallback += RemoteCertificateValidationCallback;
        
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(model.GetUri());
            request.Timeout = 50000;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            Debug.Log("Email Sended (response from server): " + responseFromServer);
            isSuccess = true;
        }
        catch (WebException ex)
        {
            Debug.LogError(ex.StackTrace);
            Debug.LogError("WebException Message: " + ex.Message);
            Debug.LogError("WebException Status: " + ex.Status);
            isSuccess = false;
        }
        catch
        {
            isSuccess = false;
        }
        finally
        {
            model.OnSended(isSuccess);
        }

        
            
    }

    private IEnumerator SendWWW(SendEmailModel model)
    {
        WWW www = new WWW(model.GetUri().AbsoluteUri);
        yield return www;
        bool isSuccess = true;
        if (!string.IsNullOrEmpty(www.error))
        {
            isSuccess = false;
            Debug.LogError(www.error);
        }
        else
        {
            Debug.LogError(www.text);
        }
        model.OnSended(isSuccess);
    }
}
