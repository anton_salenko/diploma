﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class LoadArContentCommand : BaseCommand {

    public override void Execute()
    {
        var trackable = eventData.data as TrackableBehaviour;
        
        if (trackable == null)
        {
            Debug.LogError("trackable is null");
            Fail();
            return;
        }

        Retain();
        executor.Execute(LoadObject("ARContent/Space_Rabbit"));



        
    }

    private IEnumerator LoadObject(string path)
    {
        var request = Resources.LoadAsync<GameObject>(path);
        yield return request;

        var prefab = request.asset as GameObject;

        if (prefab == null)
        {
            Fail();
            Release();
            yield break;
        }
        var arcontent = GameObject.FindObjectOfType<ARContentView>().transform;
        GameObject go = GameObject.Instantiate(prefab, arcontent);

        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;

        Release();
    }


}
