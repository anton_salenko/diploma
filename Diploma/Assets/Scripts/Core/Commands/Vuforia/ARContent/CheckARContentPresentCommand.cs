﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckARContentPresentCommand : BaseCommand
{

    public override void Execute()
    {
        var arcontent = GameObject.FindObjectOfType<ARContentView>();
        if (arcontent.transform.childCount != 0)
        {
            Fail();
            return;
        }
    }
}
