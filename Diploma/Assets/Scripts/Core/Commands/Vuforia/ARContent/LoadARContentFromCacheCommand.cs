﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using strange.extensions.dispatcher.api;
using UnityEngine.UI;

public class LoadARContentFromCacheCommand : BaseCommand
{
    [Inject] public AppDataModel appData { get; private set; } 

    private ARContentModel arContent = null;
    private TrackableBehaviour trackableBehaviour = null;
    private AssetBundle assetBundle = null;
    private Coroutine loading = null;

    public override void Execute()
    {
        
        trackableBehaviour = eventData.data as TrackableBehaviour;
        
        var eventHandler = trackableBehaviour.GetComponent<StrangeIOCTrackableEventHandler>();
        if (eventHandler == null)
        {
            Fail();
            return;
        }

        var arcontent = GameObject.FindObjectOfType<ARContentView>().transform;
        if (arcontent.childCount > 0)
        {
            if (appData.openedAnimation == trackableBehaviour)
            {
                Debug.LogError("content instantiated");
                return;
            }
            else
            {
                dispatcher.Dispatch(Events.ClearARContent);
            }
        }

        int id = eventHandler.ImageTargetID;


        appData.Animations.TryGetValue(id, out arContent);

        appData.openedAnimation = trackableBehaviour;
        Retain();
        dispatcher.AddListener(Events.ClearARContent, OnClearARContent);
        arContent.HasPlayed = true;
        appData.SelectCollection(arContent.Collection);
        loading = executor.Execute(LoadAsync());
    }

    private IEnumerator LoadAsync()
    {
        //dispatcher.Dispatch(Events.E_StartLoadARcontent);
        //dispatcher.Dispatch(Events.E_StopImageTracker);
        if (appData.assetBundle != null)
        {
            appData.assetBundle.Unload(false);

        }
        AssetBundleCreateRequest bundleLoadRequest = AssetBundle.LoadFromFileAsync(arContent.BundlePath);

        yield return bundleLoadRequest;
        assetBundle = bundleLoadRequest.assetBundle;
        bundleLoadRequest = null;
        
        if (assetBundle == null)
        {
            Debug.LogError("Failed to load AssetBundle from file");
            //dispatcher.RemoveListener(Events.E_VFClearARContent, OnClearARContent);
            Release();
            Fail();
            //dispatcher.Dispatch(Events.E_StartImageTracker);
            yield break;
        }

        AssetBundleRequest assetLoadRequest = assetBundle.LoadAllAssetsAsync<GameObject>();
        yield return assetLoadRequest;
        
        GameObject prefab = assetLoadRequest.asset as GameObject;

        Instantiate(prefab);
        //
        assetLoadRequest = null;
        prefab = null;
        AssetBundleRequest requestAudioClips = assetBundle.LoadAllAssetsAsync<AudioClip>();
        yield return requestAudioClips;
        Debug.Log("Clips Count: " + requestAudioClips.allAssets.Length);
        for (int i = 0; i < requestAudioClips.allAssets.Length; i++)
        {
            var audioFile = requestAudioClips.allAssets[i] as AudioClip;
            Debug.Log("Audio Intial LoadState: " + audioFile.loadState);
            if (audioFile.loadState == AudioDataLoadState.Unloaded || audioFile.loadState == AudioDataLoadState.Failed)
            {
                audioFile.LoadAudioData();
            }
            Debug.Log("Audio Final LoadState: " + audioFile.loadState);
        }
        requestAudioClips = null;
        appData.assetBundle = assetBundle;
        Resources.UnloadUnusedAssets();
        //dispatcher.Dispatch(EventGlobal.E_StartImageTracker);
        dispatcher.RemoveListener(Events.ClearARContent, OnClearARContent);
        Release();
    }


    private void Instantiate(GameObject prefab)
    {
        if (!instantiate)
        {
            dispatcher.RemoveListener(Events.ClearARContent, OnClearARContent);
            Release();
            Fail();
            return;
        }
        Vector3 position = trackableBehaviour.gameObject.transform.position;
        Vector3 size = new Vector3(1.0f, 1.0f, 1.0f);
        Quaternion rotation = trackableBehaviour.gameObject.transform.rotation;

        if (trackableBehaviour.gameObject != null)
        {
            StrangeIOCTrackableEventHandler siocTEH = trackableBehaviour.gameObject.GetComponent<StrangeIOCTrackableEventHandler>();
            if (siocTEH != null)
            {
                position += siocTEH.CostumePosition;
                rotation.eulerAngles = rotation.eulerAngles + siocTEH.CostumeRotation;
                size = siocTEH.CostumeSize;
            }
        }

        GameObject cam = GameObject.Find("ARCamera");
        GameObject obj = null;
        foreach (Transform gotransgorm in cam.transform)
        {
            if (gotransgorm.gameObject.name == "ARContent")
            {
                obj = gotransgorm.gameObject;
            }
        }
        var arcontent = GameObject.FindObjectOfType<ARContentView>().transform;

        if (arcontent != null)
        {
            
            GameObject clone = GameObject.Instantiate(prefab, position, rotation, arcontent) as GameObject;
            // #begin kostyl 
            var UI = clone.transform.Find("UI");
            if (UI != null)
            {
                var canvas = UI.GetComponent<CanvasScaler>();
                if (canvas != null)
                {
                    canvas.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
                    canvas.referenceResolution = new Vector2(1536, 2436);
                }
            }
            // #end kostyl
            clone.transform.localScale = size;
            clone.name = trackableBehaviour.TrackableName;

            //dispatcher.Dispatch(Events.E_EndLoadARcontent);

        }
    }

    bool instantiate = true;

    private void OnClearARContent()
    {
        Debug.LogWarning("clearaarar");
        if (assetBundle != null)
        {
            
        }
        if (loading != null)
        {
            //Executer.StopExecution(loading);
        }
        instantiate = false;
    }



}