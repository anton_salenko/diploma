﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearARContentCommand : BaseCommand {

    public override void Execute()
    {
        var arcontent = GameObject.FindObjectOfType<ARContentView>();
        foreach (Transform ts in arcontent.transform)
        {
            GameObject.Destroy(ts.gameObject);
        }
    }

}
