﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;


public class TrackingActivateCommand : BaseCommand {

    public override void Execute()
    {
        VuforiaRuntime.Instance.InitVuforia();
        TrackerManager.Instance.InitTracker<ObjectTracker>();
        TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
        Camera.main.GetComponent<VuforiaBehaviour>().enabled = true;

    }

}
