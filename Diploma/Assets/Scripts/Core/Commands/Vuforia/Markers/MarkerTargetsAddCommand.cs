﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerTargetsAddCommand : BaseCommand {

    private const string NAME = "MarkerTargets";
    private const string PATH = "Prefabs/Vuforia/MarkerTargets";

    public override void Execute()
    {
        var prefab = Resources.Load<GameObject>(PATH);
        if (prefab == null)
        {
            Debug.LogError("prefab is null at " + PATH);
            Fail();
        }

        var GO = GameObject.Instantiate<GameObject>(prefab);
        GO.name = NAME;
        GO.transform.position = Vector3.zero;

        var view = GO.GetComponent<MarkerTargetsView>();
        if (view == null)
        {
            Debug.LogError("GO has no view at " + PATH);
            Fail();
        }

    }
}
