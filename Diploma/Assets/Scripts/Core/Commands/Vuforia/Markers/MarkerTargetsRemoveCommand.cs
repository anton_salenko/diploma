﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerTargetsRemoveCommand : BaseCommand {

    private const string VIEW_NAME = "MarkerTargets";
    public override void Execute()
    {
        var view = GameObject.Find(VIEW_NAME);

        if (view == null)
        {
            Fail();
            return;
        }

        GameObject.Destroy(view);
        view = null;
    }
}
