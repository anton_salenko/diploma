﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AddAllBooksMarkersCommand : BaseCommand
{

    [Inject] public AppDataModel appData { get; private set; }

    public override void Execute()
    {
        Retain();
        executor.Execute(AddMarkers());
    }

    private IEnumerator AddMarkers()
    {

        var books = appData.Books.Values;
        if (books == null)
        {
            Fail();
            yield break;
        }
        yield return new WaitForSeconds(1f);
        TrackerManager.Instance.InitTracker<ObjectTracker>();
        TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
        ObjectTracker objTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

        GameObject arMarkers = GameObject.Find("MarkerTargets");

        objTracker.Stop();
        yield return new WaitForSeconds(1f);
        foreach (var book in books)
        {
            if (!book.CheckAllAnimationsPresent())
            {
                continue;
            }
            DataSet dataSet = objTracker.CreateDataSet();

            if (book.DataSetXMLExist)
            {
                if (dataSet.Load(book.DataSetXMLPath, VuforiaUnity.StorageType.STORAGE_ABSOLUTE))
                {

                    IEnumerable<TrackableBehaviour> tbs = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();
                    foreach (TrackableBehaviour tb in tbs)
                    {

                        if (tb.name == "New Game Object")
                        {
                            var imageTarget = tb as ImageTargetBehaviour;

                            if (imageTarget != null)
                            {
                                float aspectRatio = imageTarget.GetSize().x / imageTarget.GetSize().y;
                                float sizeMarker = 1f;
                                imageTarget.SetHeight(sizeMarker);
                                imageTarget.SetWidth(sizeMarker * aspectRatio);
                            }
                            var animation = book.Animations.Find((x) => x.Name == tb.TrackableName);
                            tb.gameObject.name = animation == null ? "" : animation.ID.ToString() + "(" + tb.TrackableName + ")";
                            var eventHandler = tb.gameObject.AddComponent<StrangeIOCTrackableEventHandler>();
                            eventHandler.ImageTargetID = animation == null ? 0 : animation.ID;
                            eventHandler.dispatcher = dispatcher;

                            //eventHandler.CostumeSize = tb.


                            if (arMarkers != null)
                                tb.transform.parent = arMarkers.transform;

                            tb.transform.localScale = Vector3.one;
                            tb.transform.localPosition = Vector3.zero;
                        }
                    }
                    if (dataSet == null)
                    {
                        Debug.LogError("dataset is null");
                    }
                    book.SetDataSet(dataSet);
                    objTracker.ActivateDataSet(dataSet);
                }
                else
                {
                    Debug.LogError("cant load dataset");

                }

            }
            else
            {
                Debug.LogError("dataset not exist");

            }
        }
        yield return new WaitForSeconds(1f);
        objTracker.Start();
        dispatcher.Dispatch(Events.E_MarkersAdded);
        Release();
    }

}
