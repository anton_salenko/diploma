﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TrackingDeactivateCommand : BaseCommand
{

    public override void Execute()
    {
        Camera.main.GetComponent<VuforiaBehaviour>().enabled = false;
        ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        tracker.Stop();
    }
}
