﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class UpdateBooksCommand : BaseCommand
{

    [Inject] public INetworkService networkService { get; set; }

    private Queue<FileRequestModel> requestQueue = new Queue<FileRequestModel>();

    private List<FileRequestModel> executingRequests = new List<FileRequestModel>();

    private int paralelRequestsCount = 10;

    private int successRequests = 0;

    private int totalRequests = 0;

    private Coroutine progressWorker = null;

    public override void Execute()
    {
        var toUpdate = eventData.data as IEnumerable<CollectionModel>;

        requestQueue = new Queue<FileRequestModel>();
        executingRequests = new List<FileRequestModel>();
        successRequests = 0;
        totalRequests = 0;
        progressWorker = null;
        
        foreach (var book in toUpdate)
        {
            if (!Directory.Exists(book.CacheDirectoryPath))
            {
                Directory.CreateDirectory(book.CacheDirectoryPath);
            }
            if (!book.DataSetDatExist)
            {
                FileRequestModel request = new FileRequestModel(book.DataSetDatUrl, (req) => SaveDat(req, book.DataSetDatPath), errorHandler: OnError, priority: ThreadPriority.High);
                requestQueue.Enqueue(request);
            }
            if (!book.DataSetXMLExist)
            {
                FileRequestModel request = new FileRequestModel(book.DataSetXMLUrl, (req) => SaveXML(req, book.DataSetXMLPath), errorHandler: OnError, priority: ThreadPriority.High);
                requestQueue.Enqueue(request);
            }
            if (!book.ImageExist)
            {
                FileRequestModel request = new FileRequestModel(book.ImageUrl, (req) => SaveImage(req, book.ImagePath), errorHandler: OnError, priority: ThreadPriority.High);
                requestQueue.Enqueue(request);
            }
            if (!book.MapExist)
            {
                FileRequestModel request = new FileRequestModel(book.MapUrl, (req) => SaveImage(req, book.MapPath), errorHandler: OnError, priority: ThreadPriority.High);
                requestQueue.Enqueue(request);
            }
        }

        totalRequests = requestQueue.Count;
        paralelRequestsCount = Mathf.Min(paralelRequestsCount, totalRequests);

        if (totalRequests <= 0)
        {
            dispatcher.Dispatch(Events.E_UpdateBooksDataSuccess);
            return;
        }

        for (int i = 0; i < paralelRequestsCount; i++)
        {
            var request = requestQueue.Dequeue();
            executingRequests.Add(request);
            dispatcher.Dispatch(Events.E_DownloadFile, request);
        }

        progressWorker = executor.Execute(CalculateTotalProgress());

    }

    private void SaveImage(FileRequestModel request, string path)
    {
        OnRequestCompleted(request);
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        File.WriteAllBytes(path, request.rawFile);

    }

    private void SaveDat(FileRequestModel request, string path)
    {
        OnRequestCompleted(request);
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        File.WriteAllBytes(path, request.rawFile);
    }

    private void SaveXML(FileRequestModel request, string path)
    {
        OnRequestCompleted(request);
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        File.WriteAllBytes(path, request.rawFile);
    }

    private void OnRequestCompleted(FileRequestModel request)
    {
        successRequests++;
        executingRequests.Remove(request);
        if (requestQueue.Count > 0)
        {
            var newRequest = requestQueue.Dequeue();
            executingRequests.Add(newRequest);
            dispatcher.Dispatch(Events.E_DownloadFile, newRequest);
        }

        if (executingRequests.Count <= 0 || successRequests == totalRequests)
        {
            Debug.Log("Log executingRequests on success: " + executingRequests.Count);
            OnLoadSuccess();
        }
    }

    private void OnLoadSuccess()
    {
        if (dispatcher.HasListener(Events.E_RetryDownloading, Execute))
        {
            dispatcher.RemoveListener(Events.E_RetryDownloading, Execute);
        }
        dispatcher.Dispatch(Events.E_UpdateBooksDataSuccess);
    }

    private void OnError(string error)
    {
        executor.StopExecution(progressWorker);
        networkService.AbortAllRequests();
        dispatcher.Dispatch(Events.E_UpdateBooksDataFail);

        if (!dispatcher.HasListener(Events.E_RetryDownloading, Execute))
        {
            dispatcher.AddListener(Events.E_RetryDownloading, Execute);
        }
    }

    private IEnumerator CalculateTotalProgress()
    {
        while (true)
        {
            if (executingRequests.Count <= 0 || totalRequests == successRequests)
            {
                yield break;
            }

            float totalProgress = successRequests;
            foreach (var request in executingRequests)
            {
                totalProgress += request.Progress;
            }
            dispatcher.Dispatch(Events.E_UpdateBooksDataProgress, totalProgress / totalRequests);
            Debug.Log("E_UpdateBooksDataProgress: " + totalProgress / totalRequests);
            yield return null;
        }
    }

}
