﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

public class LoadConfigCommand : BaseCommand
{

    public string configPath = Path.Combine(Application.persistentDataPath, "config.json");

    private bool localConfigLoaded = false;
    private bool remoteConfigLoaded = false;

    private Dictionary<int, CollectionModel> booksParseBuffer = new Dictionary<int, CollectionModel>(); 
    private Dictionary<int, ARContentModel> animationsParseBuffer = new Dictionary<int, ARContentModel>();

    private IEnumerable<CollectionModel> booksToUpdate = null;
    private IEnumerable<ARContentModel> animationsToUpdate = null;

    [Inject] public AppDataModel appDataModel { get; set; }

    public override void Execute()
    {
        if (dispatcher.HasListener(Events.E_RetryDownloading, Execute))
        {
            dispatcher.RemoveListener(Events.E_RetryDownloading, Execute);
        }
        LoadLocalConfigs();

        if (localConfigLoaded)
        {
            dispatcher.Dispatch(Events.E_LocalConfigLoadSuccess);
            appDataModel.SetBooks(booksParseBuffer.Values);
            appDataModel.SetAnimations(animationsParseBuffer.Values);
        }
        else
        {
            dispatcher.Dispatch(Events.E_LocalConfigLoadFailed);
        }

        LoadRemoteConfigs();


    }

    private void LoadLocalConfigs()
    {
        if (!File.Exists(configPath))
        {
            localConfigLoaded = false;
            return;
        }

        string jsonString = File.ReadAllText(configPath);
        if (string.IsNullOrEmpty(jsonString))
        {
            localConfigLoaded = false;
            return;
        }

        localConfigLoaded = ParseConfigs(jsonString);

    }

    private void LoadRemoteConfigs()
    {
        JSONRequestModel request = new JSONRequestModel(NetworkRequests.GetJsonData, OnRemoteLoadSuccess, errorHandler: OnNetworkError);
        dispatcher.Dispatch(Events.E_DownloadJSON, request);
    }

    private List<CollectionModel> SelectBooksToUpdate(Dictionary<int, CollectionModel> localBooks, Dictionary<int, CollectionModel> remoteBooks)
    {
        List<CollectionModel> toUpdate = new List<CollectionModel>();
        foreach (var remoteBook in remoteBooks)
        {
            if (!localBooks.ContainsKey(remoteBook.Key))
            {
                Debug.Log("Need update book " + remoteBook.Key);
                remoteBook.Value.ClearAllLocalFiles();
                toUpdate.Add(remoteBook.Value);
                continue;
            }
            CollectionModel localBook = localBooks[remoteBook.Key];
            if (localBook.Version != remoteBook.Value.Version)
            {
                Debug.Log("Need update book " + remoteBook.Key);
                remoteBook.Value.ClearAllLocalFiles();
                toUpdate.Add(remoteBook.Value);
                continue;
            }

            if (!remoteBook.Value.DataSetDatExist || !remoteBook.Value.ImageExist || !remoteBook.Value.DataSetXMLExist || !remoteBook.Value.MapExist)
            {
                toUpdate.Add(remoteBook.Value);
                continue;
            }
        }
        return toUpdate;
    }

    private List<ARContentModel> SelectAnimationsToUpdate(Dictionary<int, ARContentModel> localAnimations, Dictionary<int, ARContentModel> remoteAnimations)
    {
        List<ARContentModel> toUpdate = new List<ARContentModel>();
        foreach (var remoteAnimation in remoteAnimations)
        {
            if (!localAnimations.ContainsKey(remoteAnimation.Key))
            {
                Debug.Log("Need update book " + remoteAnimation.Key);
                remoteAnimation.Value.ClearAllLocalFiles();
                toUpdate.Add(remoteAnimation.Value);
                continue;
            }
            ARContentModel localAnimation = localAnimations[remoteAnimation.Key];
            if (localAnimation.Version != remoteAnimation.Value.Version)
            {
                Debug.Log("Need update book " + remoteAnimation.Key);
                remoteAnimation.Value.ClearAllLocalFiles();
                toUpdate.Add(remoteAnimation.Value);
            }
        }
        return toUpdate;
    }


    public const string COLLECTIONS_KEY = "collection";
    public const string ANIMATIONS_KEY = "arcontent";

    private bool ParseConfigs(string jsonString)
    {
        if (string.IsNullOrEmpty(jsonString))
        {
            return false;
        }
        JSONNode json = JSONCreator.Parse(jsonString);

        JSONArray books = json[COLLECTIONS_KEY].AsArray;
        JSONArray animations = json[ANIMATIONS_KEY].AsArray;

        booksParseBuffer.Clear();
        foreach (JSONNode bookJSON in books)
        {
            
            var book = new CollectionModel(bookJSON);
            booksParseBuffer.Add(book.ID, book);
            Debug.Log(book.ID);
        }

        animationsParseBuffer.Clear();
        foreach (JSONNode animationJSON in animations)
        {
            var animation = new ARContentModel(animationJSON);
            if (!booksParseBuffer.ContainsKey(animation.CollectionID))
            {
                Debug.LogError("Cant find book for animation: " + animation.ID);
                continue;
            }
            var book = booksParseBuffer[animation.CollectionID];
            animation.SetCollection(book);
            book.AddAnimation(animation);
            animationsParseBuffer.Add(animation.ID, animation);
        }
        return booksParseBuffer.Count > 0 && animationsParseBuffer.Count > 0;
    }

    private void OnNetworkError(string error)
    {
        remoteConfigLoaded = false;
        if (!localConfigLoaded)
        {
            // load again
            dispatcher.Dispatch(Events.E_RemoteConfigLoadFailed);
            dispatcher.AddListener(Events.E_RetryDownloading, Execute);
        }
        else
        {
            dispatcher.Dispatch(Events.E_StartUsingLocalConfigs);
        }
    }

    private void OnRemoteLoadSuccess(string jsonString)
    {
        remoteConfigLoaded = ParseConfigs(jsonString);
        if (File.Exists(configPath))
        {
            File.Delete(configPath);
        }
        File.WriteAllText(configPath, jsonString);


        if (remoteConfigLoaded)
        {
            dispatcher.Dispatch(Events.E_RemoteConfigLoadSuccess);
        }
        else
        {
            dispatcher.Dispatch(Events.E_RemoteConfigLoadFailed);
            if (!localConfigLoaded)
            {
                // load again
                dispatcher.AddListener(Events.E_RetryDownloading, Execute);
            }
            return;
        }

        if (remoteConfigLoaded)
        {

            if (localConfigLoaded)
            {
                booksToUpdate = SelectBooksToUpdate(appDataModel.Books, booksParseBuffer);
                animationsToUpdate = SelectAnimationsToUpdate(appDataModel.Animations, animationsParseBuffer);
            }
            else
            {
                booksToUpdate = appDataModel.Books.Values;
                animationsToUpdate = appDataModel.Animations.Values;
            }

            appDataModel.SetBooks(booksParseBuffer.Values);
            appDataModel.SetAnimations(animationsParseBuffer.Values);


            dispatcher.Dispatch(Events.E_UpdateBooksData, booksToUpdate);
        }

    }

}
