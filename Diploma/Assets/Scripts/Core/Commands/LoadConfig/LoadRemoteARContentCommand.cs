﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadRemoteARContentCommand : BaseCommand
{

    [Inject] public INetworkService networkService { get; set; }

    private Queue<FileRequestModel> requestQueue = new Queue<FileRequestModel>();

    private List<FileRequestModel> executingRequests = new List<FileRequestModel>();

    private int paralelRequestsCount = 5;

    private int successRequests = 0;

    private int totalRequests = 0;

    private Coroutine progressWorker = null;

    CollectionModel book = null;


    public override void Execute()
    {
        Debug.Log("Execute");
        if (book == null)
        {
            book = eventData.data as CollectionModel;
        }

        if (book == null)
        {
            Fail();
            return;

        }
        Debug.Log("Execute");

        foreach (var animation in book.Animations)
        {
            if (!Directory.Exists(animation.CacheDirectoryPath))
            {
                Directory.CreateDirectory(animation.CacheDirectoryPath);
            }

            if (!animation.BundleExist)
            {
                FileRequestModel request = new FileRequestModel(animation.BundleUrl, (req) => SaveBundle(req, animation.BundlePath), errorHandler: OnError, priority: ThreadPriority.High);
                requestQueue.Enqueue(request);
            }
        }

        totalRequests = requestQueue.Count;
        paralelRequestsCount = Mathf.Min(paralelRequestsCount, totalRequests);

        if (totalRequests <= 0)
        {
            dispatcher.Dispatch(Events.E_LoadingRemoteARContentSuccess, book);
            return;
        }

        for (int i = 0; i < paralelRequestsCount; i++)
        {
            var request = requestQueue.Dequeue();
            executingRequests.Add(request);
            dispatcher.Dispatch(Events.E_DownloadFile, request);
        }

        dispatcher.AddListener(Events.E_CancelDownloading, AbortRequests);
        progressWorker = executor.Execute(CalculateTotalProgress());
        Debug.Log("Execute");

    }

    private void SaveBundle(FileRequestModel request, string path)
    {
        OnRequestCompleted(request);
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        if (request.rawFile != null && request.rawFile.Length > 0)
        {
            File.WriteAllBytes(path, request.rawFile);
        }
        else
        {

            Debug.LogError("file is empty " + request.URL);
        }
        request.Dispose();
    }

    private void OnRequestCompleted(FileRequestModel request)
    {
        successRequests++;
        executingRequests.Remove(request);
        if (requestQueue.Count > 0)
        {
            var newRequest = requestQueue.Dequeue();
            executingRequests.Add(newRequest);
            dispatcher.Dispatch(Events.E_DownloadFile, newRequest);
        }

        if (executingRequests.Count <= 0 || successRequests == totalRequests)
        {
            Debug.Log("Log executingRequests on success: " + executingRequests.Count);
            OnLoadSuccess();
        }
    }

    private void OnLoadSuccess()
    {
        RemoveListeners();
        dispatcher.Dispatch(Events.E_LoadingRemoteARContentSuccess, book);
    }

    private void OnError(string error)
    {
        AbortRequests();
        dispatcher.Dispatch(Events.E_LoadingRemoteARContentFail, book);

        if (!dispatcher.HasListener(Events.E_RetryDownloading, Execute))
        {
            dispatcher.AddListener(Events.E_RetryDownloading, Execute);
        }
    }

    private void RemoveListeners()
    {
        if (progressWorker != null)
        {
            executor.StopExecution(progressWorker);
        }
        dispatcher.RemoveListener(Events.E_CancelDownloading, AbortRequests);

        if (dispatcher.HasListener(Events.E_RetryDownloading, Execute))
        {
            dispatcher.RemoveListener(Events.E_RetryDownloading, Execute);
        }
    }

    private IEnumerator CalculateTotalProgress()
    {
        while (true)
        {
            if (executingRequests.Count <= 0 || totalRequests == successRequests)
            {
                yield break;
            }

            float totalProgress = successRequests;
            foreach (var request in executingRequests)
            {
                totalProgress += request.Progress;
            }
            book.loadingProgress = totalProgress / totalRequests;
            dispatcher.Dispatch(Events.E_LoadingRemoteARContentProgress, totalProgress / totalRequests);
            Debug.Log("E_LoadingARContentProgress: " + totalProgress / totalRequests);
            yield return null;
        }
    }

    private void AbortRequests()
    {
        networkService.AbortAllRequests();
        RemoveListeners();
        Fail();

    }

}
