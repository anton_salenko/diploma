﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppStartCommand : BaseCommand {

    public override void Execute()
    {
        executor.Execute(WaitAndExecute());
    }
	
    private IEnumerator WaitAndExecute()
    {
        yield return null;
        dispatcher.Dispatch(Events.E_LoadConfigFile);
        dispatcher.Dispatch(Events.VuforiaARCamearAdd);
        dispatcher.Dispatch(Events.UILoadingAdd);
        dispatcher.Dispatch(Events.MarkersAddCommand);
        dispatcher.Dispatch(Events.ARContentAdd);
    }
}
