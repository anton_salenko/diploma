﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Events {

    None,

    // Vuforia
    VuforiaARCamearAdd,
    VuforiaARCameraEnable,
    VuforiaARCameraDisable,
    ARContentAdd,
    ClearARContent,
    //markers
    MarkersAddCommand,
    MarkersRemoveCommand,
    MarkerFounded,
    MarkerLost,
    // Vuforia end

    //ui
    UILoadingAdd,
    UILoadingRemove,
    UIMainMenuAdd,
    UIMainMenuRemove,
    UIHUDAdd,
    UIHUDRemove,
    UISettingsAdd,
    UISettingsRemove,
    UICollectionsAdd,
    UICollectionsRemove,
    UIMapShow,
    UIMapHide,
    //ui end
    SFXOnVolumeChanged,

    E_AppBackButton,
    E_Swipe,
    E_Zoom,
    E_InputBeginDrag,
    E_InputDrag,
    E_InputEndDrag,
    E_InputTap,
    E_InputHold,

    E_RetryDownloading,
    E_LocalConfigLoadSuccess,
    E_RemoteConfigLoadFailed,
    E_StartUsingLocalConfigs,
    E_RemoteConfigLoadSuccess,
    E_UpdateBooksData,
    E_LoadConfigFile,
    E_LocalConfigLoadFailed,
    E_DownloadJSON,
    E_UpdateBooksDataSuccess,
    E_DownloadFile,
    E_DownloadTexture,
    E_DownloadAsset,
    E_PostEmail,
    E_LoadRemoteARContent,
    E_UpdateBooksDataFail,
    E_UpdateBooksDataProgress,
    E_LoadingRemoteARContentSuccess,
    E_CancelDownloading,
    E_LoadingRemoteARContentFail,
    E_LoadingRemoteARContentProgress,
    E_BadNetworkPopUpShow,
    E_BadNetworkPopUpHide,

    E_AddAllMarkers,
    E_MarkersAdded,
    E_VFTrackableARMarkerTargetsLost,
    E_VFTrackableARMarkerTargetsFound,

    EnableBlur,
    DisableBlur,

    End
}
