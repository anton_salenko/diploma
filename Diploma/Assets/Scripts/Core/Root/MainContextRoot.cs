﻿using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainContextRoot : MVCSContext
{

    public MainContextRoot(MonoBehaviour view) : base(view)
    {

    }

    public MainContextRoot(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
    {
    }

    protected override void mapBindings()
    {
        injectionBinder.Bind<IExecutor>().To<Executor>().ToSingleton();
        injectionBinder.Bind<AppDataModel>().ToSingleton();
        injectionBinder.Bind<SFXManager>().ToSingleton();
        injectionBinder.Bind<INetworkService>().To<NetworkService>().ToSingleton();


        mediationBinder.BindView<ARCameraView>().ToMediator<ARCameraMediator>();
        mediationBinder.BindView<MarkerTargetsView>().ToMediator<MarkerTargetsMediator>();
        mediationBinder.BindView<MarkerEventsTrackerView>().ToMediator<MarkerEventsTrackerMediator>();
        mediationBinder.BindView<ARContentView>().ToMediator<ARContentMediator>();
        mediationBinder.BindView<SFXSoundVolumeBehaviour>().ToMediator<SFXSoundVolumeMediator>();


        commandBinder.Bind(Events.VuforiaARCamearAdd).To<VuforiaARCameraAddCommand>().Pooled();

        //The START event is fired as soon as mappings are complete.
        //Note how we've bound it "Once". This means that the mapping goes away as soon as the command fires.
        commandBinder.Bind(ContextEvent.START).To<AppStartCommand>().Once().Pooled();

        commandBinder.Bind(Events.VuforiaARCameraEnable).To<TrackingActivateCommand>().Pooled();
        commandBinder.Bind(Events.VuforiaARCameraDisable).To<TrackingDeactivateCommand>().Pooled();
        commandBinder.Bind(Events.MarkersAddCommand).To<MarkerTargetsAddCommand>().Pooled();
        commandBinder.Bind(Events.MarkersRemoveCommand).To<MarkerTargetsRemoveCommand>().Pooled();

        commandBinder.Bind(Events.MarkerFounded).To<CheckARContentPresentCommand>().To<LoadArContentCommand>().Pooled();
        commandBinder.Bind(Events.ARContentAdd).To<ARContentAddCommand>().Pooled();
        commandBinder.Bind(Events.ClearARContent).To<ClearARContentCommand>().Pooled();

        //network
        commandBinder.Bind(Events.E_DownloadJSON).To<NetworkGetJSONCommand>().InSequence().Pooled();
        commandBinder.Bind(Events.E_DownloadFile).To<NetworkLoadFileCommand>().InSequence().Pooled();
        commandBinder.Bind(Events.E_DownloadTexture).To<NetworkLoadTextureCommand>().InSequence().Pooled();
        commandBinder.Bind(Events.E_DownloadAsset).To<NetworkLoadAssetCommand>().InSequence().Pooled();
        commandBinder.Bind(Events.E_PostEmail).To<NetworkPostEmailCommand>().InSequence().Pooled();
        commandBinder.Bind(Events.E_UpdateBooksData).To<UpdateBooksCommand>().Pooled();
        commandBinder.Bind(Events.E_LoadConfigFile).To<LoadConfigCommand>().Pooled();
        commandBinder.Bind(Events.E_AddAllMarkers).To<AddAllBooksMarkersCommand>().Pooled();
            
        commandBinder.Bind(Events.E_LoadRemoteARContent).To<LoadRemoteARContentCommand>().Pooled();
        commandBinder.Bind(Events.E_VFTrackableARMarkerTargetsFound).To<LoadARContentFromCacheCommand>().Pooled();

        commandBinder.Bind(Events.E_BadNetworkPopUpShow).To<UIBadNetworkConnectionPopUpShowCommand>().Pooled();
        commandBinder.Bind(Events.E_BadNetworkPopUpHide).To<UIBadNetworkConnectionPopUpHideCommand>().Pooled();
        commandBinder.Bind(Events.E_LoadingRemoteARContentFail).To<UIBadNetworkConnectionPopUpShowCommand>().Pooled();
        commandBinder.Bind(Events.E_RemoteConfigLoadFailed).To<UIBadNetworkConnectionPopUpShowCommand>().Pooled();
        commandBinder.Bind(Events.E_UpdateBooksDataFail).To<UIBadNetworkConnectionPopUpShowCommand>().Pooled();

        //

        //ui
        mediationBinder.BindView<UILoadingView>().ToMediator<UILoadingMediator>();
        mediationBinder.BindView<UIMainMenuView>().ToMediator<UIMainMenuMediator>();
        mediationBinder.BindView<UIHUDView>().ToMediator<UIHUDMediator>();
        mediationBinder.BindView<UISettingsView>().ToMediator<UISettingsMediator>();
        mediationBinder.BindView<UICollectionsView>().ToMediator<UICollectionsMediator>();
        mediationBinder.BindView<UICollectionView>().ToMediator<UICollectionMediator>();
        mediationBinder.BindView<UIBadNetworkPopUpView>().ToMediator<UIBadNetworkPopUpMediator>();
        mediationBinder.BindView<UIMapView>().ToMediator<UIMapMediator>();


        commandBinder.Bind(Events.UILoadingAdd).To<UILoadingViewAddCommand>().Pooled();
        commandBinder.Bind(Events.UILoadingRemove).To<UILoadingViewRemoveCommand>().Pooled();
        commandBinder.Bind(Events.UIMainMenuAdd).To<UIMainMenuAddCommand>().Pooled();
        commandBinder.Bind(Events.UIMainMenuRemove).To<UIMainMenuRemoveCommand>().Pooled();
        commandBinder.Bind(Events.UIHUDAdd).To<UIHUDAddCommand>().Pooled();
        commandBinder.Bind(Events.UIHUDRemove).To<UIHUDRemoveCommand>().Pooled();
        commandBinder.Bind(Events.UISettingsAdd).To<UISettingsAddCommand>().Pooled();
        commandBinder.Bind(Events.UISettingsRemove).To<UISettingsRemoveCommand>().Pooled();
        commandBinder.Bind(Events.UICollectionsAdd).To<UICollectionsAddCommand>().Pooled();
        commandBinder.Bind(Events.UICollectionsRemove).To<UICollectionsRemoveCommand>().Pooled();
        commandBinder.Bind(Events.UIMapShow).To<UIMapShowCommand>().Pooled();
        commandBinder.Bind(Events.UIMapHide).To<UIMapHideCommand>().Pooled();

        //ui end

    }


    //You can safely ignore this bit. Since changing our default to Signals from Events, this is now necessary in this example.
    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>(); //Unbind to avoid a conflict!
        injectionBinder.Bind<ICommandBinder>().To<EventCommandBinder>().ToSingleton();


        
    }
}
