﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Vuforia;
using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.context.api;



namespace Vuforia
{
    public class StrangeIOCTrackableEventHandler : MarkerEventsTrackerView
    {
        public Vector3 CostumePosition = new Vector3();
        public Vector3 CostumeRotation = new Vector3();
        public Vector3 CostumeSize = new Vector3(1.0f, 1.0f, 1.0f);
        public int ImageTargetID { get; set; }
        //public Vector3 CostumeRotation = new Vector3();
        
        public IEventDispatcher dispatcher { get; set; }


        protected override void OnTrackingFound()
        {
            Debug.Log("found");
            if (dispatcher != null)
            {
                dispatcher.Dispatch(Events.E_VFTrackableARMarkerTargetsFound, mTrackableBehaviour);
                //dispatcher.Dispatch(EventGlobal.E_UICanvasInteractiveHintHide);
            }
            else
            {
                MainContextView.DispatchStrangeEvent(Events.E_VFTrackableARMarkerTargetsFound, mTrackableBehaviour);
                // MainContextView.DispatchStrangeEvent(EventGlobal.E_UICanvasInteractiveHintHide);
            }
        }

        protected override void OnTrackingLost()
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            if (dispatcher != null)
            {
                dispatcher.Dispatch(Events.E_VFTrackableARMarkerTargetsLost, mTrackableBehaviour);
                //dispatcher.Dispatch(EventGlobal.E_UICanvasInteractiveHintShow);
            }
            else
            {
                MainContextView.DispatchStrangeEvent(Events.E_VFTrackableARMarkerTargetsLost, mTrackableBehaviour);
                // MainContextView.DispatchStrangeEvent(EventGlobal.E_UICanvasInteractiveHintShow);
            }
        }
    }
}
