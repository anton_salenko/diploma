﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlurPostEffect : MonoBehaviour
{

	public Material postEffectMat;

	//[Range(1f/16f, 1)]
	//public float size;

	[Range( 1f, 32f )]
	public float power;

	private void OnRenderImage( RenderTexture source, RenderTexture destination )
	{
		var temp1 = RenderTexture.GetTemporary( Mathf.RoundToInt( source.width * ( 1 - ( power / 56 ) ) ), Mathf.RoundToInt( source.height * ( 1 - ( power / 56 ) ) ), 0 );
		var temp2 = RenderTexture.GetTemporary( Mathf.RoundToInt( source.width * ( 1 - ( power / 56 ) ) ), Mathf.RoundToInt( source.height * ( 1 - ( power / 56 ) ) ), 0 );

		postEffectMat.SetFloat( "_Power", power );

		Graphics.Blit( source, temp1 );
		Graphics.Blit( temp1, temp2, postEffectMat, 0 );
		Graphics.Blit( temp2, temp1, postEffectMat, 1 );
		Graphics.Blit( temp1, temp2, postEffectMat, 0 );
		Graphics.Blit( temp2, temp1, postEffectMat, 1 );
		Graphics.Blit( temp1, destination );
		RenderTexture.ReleaseTemporary( temp1 );
		RenderTexture.ReleaseTemporary( temp2 );
	}
}
