﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapPointBehaviour : MonoBehaviour {

    [SerializeField] GameObject defaultImage;
    [SerializeField] GameObject activeImage;
    [SerializeField] Text label;

    public void SetActive(bool active)
    {
        defaultImage.SetActive(!active);
        activeImage.SetActive(active);
    }

    public void SetPosition(Vector2 position)
    {
        var rect = transform as RectTransform;
        if (rect == null)
        {
            return;
        }
        rect.anchorMax = position;
        rect.anchorMin = position;
        rect.anchoredPosition = Vector2.zero;
    }

    public void SetLabel(string text)
    {
        label.text = text;
    }
	
}
