﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SFXSoundVolumeBehaviour : BaseView
{

    [SerializeField] bool isMusic = false;

    [Inject] public SFXManager sfxManager { get; set; }

	public void LoadView()
    {
        var audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.volume = isMusic ? sfxManager.MusicVolume : sfxManager.SoundVolume;
        dispatcher.AddListener(Events.SFXOnVolumeChanged, OnVolumeChanged);
    }

    public void RemoveView()
    {
        dispatcher.RemoveListener(Events.SFXOnVolumeChanged, OnVolumeChanged);
    }


    private void OnVolumeChanged()
    {
        var audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.volume = isMusic ? sfxManager.MusicVolume : sfxManager.SoundVolume;
    }
}
