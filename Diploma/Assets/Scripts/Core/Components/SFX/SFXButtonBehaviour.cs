﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(AudioSource))]
public class SFXButtonBehaviour : BaseView {

    private AudioSource audioSource;

    void Start ()
    {
        var button = gameObject.GetComponent<Button>();
        audioSource = gameObject.GetComponent<AudioSource>();
        button.onClick.AddListener(OnCkick);
	}

    private void OnCkick()
    {
        audioSource.Stop();
        audioSource.Play();
    }
	
}
