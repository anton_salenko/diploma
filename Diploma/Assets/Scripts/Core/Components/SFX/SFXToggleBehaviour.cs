﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
[RequireComponent(typeof(AudioSource))]
public class SFXToggleBehaviour : BaseView
{

    private AudioSource audioSource;

    void Start()
    {
        var button = gameObject.GetComponent<Toggle>();
        audioSource = gameObject.GetComponent<AudioSource>();
        button.onValueChanged.AddListener(OnCkick);
    }

    private void OnCkick(bool value)
    {
        audioSource.Stop();
        audioSource.Play();
    }

}