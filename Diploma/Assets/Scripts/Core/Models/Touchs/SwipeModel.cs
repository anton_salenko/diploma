﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class SwipeModel
{
	public MainContextInput.stateTouch state = MainContextInput.stateTouch.STdefault;
	public Vector3 point1 = Vector3.zero;
	public Vector3 point1Delta = Vector3.zero;
	public float EventTime = 0.0f;
}