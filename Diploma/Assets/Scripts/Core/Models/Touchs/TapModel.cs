﻿using UnityEngine;
using System;
using System.Collections.Generic;


public class TapModel
{
	public MainContextInput.stateTouch stateT;
	public Vector3 point1;
	public int tapCount;
	public int tapGlobalCount;
	public bool isTapOnDragObject;
}