﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class ZoomModel
{
	public MainContextInput.stateTouch state = MainContextInput.stateTouch.STdefault;
	public Vector3 point1 = Vector3.zero;
	public Vector3 point1Delta = Vector3.zero;
	public Vector3 point2 = Vector3.zero;
	public Vector3 point2Delta = Vector3.zero;
}