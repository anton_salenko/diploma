﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmailPostRequestModel
{

    public string Url { get; private set; }
    public Dictionary<string, string> PostData { get; private set; }
    public System.Action<bool> OnResponse { get; private set; }

    public EmailPostRequestModel(string url, Dictionary<string, string> postData, System.Action<bool> onResponse)
    {
        PostData = postData;
        Url = url;
        OnResponse = onResponse;
    }

}
