﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class SendEmailModel
{
    public string UserEmail { get; private set; }
    public string Message { get; private set; }

    private Action<bool> callback;
    private const int appId = 9;
    private const string uri = "https://cloudliveanimations.org:443/api/apps/{0}/send_email.json";

    public SendEmailModel(string userEmail, string message, Action<bool> callback)
    {
        UserEmail = userEmail;
        Message = message;

        this.callback = callback;
    }

    public Uri GetUri()
    {
        Uri result = new Uri(string.Format(uri, appId) + "?" + GetQuery());
        return result;
    }

    public void OnSended(bool isSuccess)
    {
        if (callback != null)
        {
            callback(isSuccess);
        }
    }

    private string GetQuery()
    {
        Dictionary<string, string> post = new Dictionary<string, string>();

        post.Add("api_token", "8de3eea2f7d338f9ebece1215847cf31");
        post.Add("email_type", "support");
        post.Add("email", UserEmail);

        post.Add("language_code", "en");
        post.Add("message", Message);

        var query = new StringBuilder();

        foreach (KeyValuePair<string, string> pair in post)
        {
            query.AppendFormat("{0}={1}&", pair.Key, pair.Value);
        }
        --query.Length;

        return query.ToString();
    }

}
