﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;

public class FileRequestModel : IDisposable {

    public byte[] rawFile = null;

    private System.Action<FileRequestModel> callback;
    private System.Action<float> progressCallback;
    private System.Action<string> errorHandler;
    public ThreadPriority priority;
    public JSONNode Data { get; private set; }
    public Dictionary<string, string> Headers { get; private set; }
    public string URL { get; private set; }
    public WWW www = null;
    public float Progress { get; private set; }

    public FileRequestModel(string url, System.Action<FileRequestModel> callback, System.Action<float> progressCallback = null, JSONNode data = null, Dictionary<string, string> headers = null, System.Action<string> errorHandler = null, ThreadPriority priority = ThreadPriority.Normal)
    {
        this.callback = callback;
        this.errorHandler = errorHandler;
        this.progressCallback = progressCallback;
        this.priority = priority;
        URL = url;
        Data = data;
        Headers = headers;

    }

    public void SetFile(byte[] file)
    {
        rawFile = file;
    }

    public void ProgressCallback(float progress)
    {
        Progress = progress;
        if (progressCallback != null)
        {
            progressCallback(progress);
        }
    }

    public void Callback()
    {
        callback(this);
    }

    public void Error(WWW www)
    {
        Debug.LogWarning(www.url + "\n" + www.error);
        if (errorHandler != null)
        {
            errorHandler(www.error);
        }
       
    }

    public void Dispose()
    {
        rawFile = null;
    }
}
