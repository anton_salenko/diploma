﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;


public class TextureRequestModel
{

    private Texture2D texture;

    public System.Action<Texture2D> callback;
    private System.Action<string> errorHandler;
    public JSONNode Data { get; private set; }
    public Dictionary<string, string> Headers { get; private set; }
    public string URL { get; private set; }

    public TextureRequestModel(string url, System.Action<Texture2D> callback, JSONNode data = null, Dictionary<string, string> headers = null, System.Action<string> errorHandler = null)
    {
        this.callback = callback;
        this.errorHandler = errorHandler;
        URL = url;
        Data = data;
        Headers = headers;
    }

    public void SetTexture(Texture2D texture)
    {
        this.texture = texture;
    }

    public void Callback()
    {
        if (callback != null)
        {
            callback(texture);
        }
    }

    public void Error(WWW www)
    {
        if (errorHandler != null)
        {
            errorHandler(www.error);
        }
        Debug.LogWarning(www.url + "\n" + www.error);
    }

}
