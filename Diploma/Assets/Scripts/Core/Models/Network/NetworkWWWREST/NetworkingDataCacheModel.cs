﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkingDataCacheModel
{
    public int PlayerNumber = -1;
    public int PlayerId = -1;
    public int PlayerServerID = -1;
    public bool isLoginToServer = false;
    public string RoomName = "";

    private bool isNetworkingGame = false;
    public bool isNetworking
    {
        get
        {
            return isNetworkingGame;
        }
        set
        {
            isNetworkingGame = value;
        }
    }

    public static NetworkingDataCacheModel instance;
    public NetworkingDataCacheModel()
    {
        instance = this;
    }
    ~NetworkingDataCacheModel()
    {
        instance = null;
    }

    public void UpdateStates(SimpleJSON.JSONNode jsonArray)
    {
        if (jsonArray == null)
        {
            Debug.LogError("NetworkingDataCacheModel parse ERROR jsonArray==NULL");
            return;
        }

        foreach (SimpleJSON.JSONNode node in jsonArray.AsArray)
        {
            int player_id = -1;
            int created_at = -1;

            Debug.Log("node: " + node.ToString());

            if (node["player_id"] != null)
            {
                try
                {
                    player_id = int.Parse(node["player_id"].Value);
                }
                catch
                {
                }
            }

            if (node["created_at"] != null)
            {
                try
                {
                    created_at = int.Parse(node["created_at"].Value);
                }
                catch
                {
                }
            }


            if (player_id <= 0)
            {
                Debug.LogError("NetworkingDataCacheModel  ERROR player_id:" + player_id.ToString());
            }

            if (created_at <= 0)
            {
                Debug.LogError("NetworkingDataCacheModel  ERROR created_at:" + created_at.ToString());
            }

            if (node["json"]!= null)
            {
                int PlayerNumber = -1;

                // ТУТ ТОРМОЗА
                string jsondatastring = node["json"];
                SimpleJSON.JSONNode json = SimpleJSON.JSONNode.Parse(jsondatastring);
                //

                if (json["PlayerNumber"] != null)
                {
                    PlayerNumber = json["PlayerNumber"].AsInt;
                }
                else
                {
                    Debug.LogError("NetworkingDataCacheModel json[\"PlayerNumber\"] == NULL");
                }

                int eventId = -1;
                if (json["eventId"] != null)
                {
                    eventId = json["eventId"].AsInt;
                }
                else
                {
                    Debug.LogError( "NetworkingDataCacheModel json[\"eventId\"] == NULL" );
                }
            }
            else
            {
                Debug.LogError("NetworkingDataCacheModel parse ERROR player_id:" + player_id.ToString() + " created_at:" + created_at.ToString());
            }
        }
    }

}

