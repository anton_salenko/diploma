﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class JSONRequestModel
{

    private string json;

    private System.Action<string> callback;
    private System.Action<string> errorHandler;
    public JSONNode Data { get; private set; }
    public Dictionary<string, string> Headers { get; private set; }
    public string URL { get; private set; }
    
    public JSONRequestModel(string url, System.Action<string> callback, JSONNode data = null, Dictionary<string, string> headers = null, System.Action<string> errorHandler = null)
    {
        this.callback = callback;
        this.errorHandler = errorHandler;
        URL = url;
        Data = data;
        Headers = headers;
    }

    public void SetJSON(string JSON)
    {
        json = JSON;
    }

    public void Callback()
    {
        callback(json);
    }

    public void Error(WWW www)
    {
        if (errorHandler != null)
        {
            errorHandler(www.error);
        }
        Debug.LogError(www.url + "\n" + www.error);
    }
}
