﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetRequestModel
{

    private AssetBundle asset;

    private System.Action<AssetBundle> callback;
    private System.Action<string> errorHandler;
    public JSONNode Data { get; private set; }
    public Dictionary<string, string> Headers { get; private set; }
    public string URL { get; private set; }

    public AssetRequestModel(string url, System.Action<AssetBundle> callback, JSONNode data = null, Dictionary<string, string> headers = null, System.Action<string> errorHandler = null)
    {
        this.callback = callback;
        this.errorHandler = errorHandler;
        URL = url;
        Data = data;
        Headers = headers;
    }

    public void SetFile(AssetBundle asset)
    {
        this.asset = asset;
    }

    public void Callback()
    {
        callback(asset);
    }

    public void Error(WWW www)
    {
        if (errorHandler != null)
        {
            errorHandler(www.error);
        }
        Debug.LogError(www.url + "\n" + www.error);
    }

}
