﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

public class CollectionModel
{
    public const string DATA_SET_DAT_KEY = "dataset_dat";
    public const string DATA_SET_XML_KEY = "dataset_xml";
    public const string IMAGE_KEY = "image";
    public const string NAME_KEY = "name";
    public const string VERSION_KEY = "version";
    public const string ID_KEY = "id";
    public const string MAP_KEY = "map";
    

    public string DataSetDatUrl { get; private set; }
    public string DataSetDatPath { get { return Path.Combine(CacheDirectoryPath, "data_set.dat"); } }
    public bool DataSetDatExist { get { return File.Exists(DataSetDatPath); } }

    public string DataSetXMLUrl { get; private set; }
    public string DataSetXMLPath { get { return Path.Combine(CacheDirectoryPath, "data_set.xml"); } }
    public bool DataSetXMLExist { get { return File.Exists(DataSetXMLPath); } }

    public string ImageUrl { get; private set; }
    public string ImagePath { get { return Path.Combine(CacheDirectoryPath, "image.png"); } }
    public bool ImageExist { get { return File.Exists(ImagePath); } }

    public string MapUrl { get; private set; }
    public string MapPath { get { return Path.Combine(CacheDirectoryPath, "map.png"); } }
    public bool MapExist { get { return File.Exists(MapPath); } }

    public List<ARContentModel> Animations { get; private set; }

    public string Name { get; private set; }

    public int Version { get; private set; }

    public int ID { get; private set; }

    public Vuforia.DataSet DataSet { get; private set; } 

    public string CacheDirectoryPath { get { return Path.Combine(Application.persistentDataPath, ID.ToString()); } }

    public float loadingProgress = 0;

    public CollectionModel(JSONNode json)
    {
        Animations = new List<ARContentModel>();
        ID = json[ID_KEY].AsInt;
        Name = json[NAME_KEY];
        Version = json[VERSION_KEY].AsInt;
        ImageUrl = NetworkRequests.ServerUrl + json[IMAGE_KEY];
        DataSetDatUrl = NetworkRequests.ServerUrl + json[DATA_SET_DAT_KEY];
        DataSetXMLUrl = NetworkRequests.ServerUrl + json[DATA_SET_XML_KEY];
        MapUrl = NetworkRequests.ServerUrl + json[MAP_KEY];
    }

    public void AddAnimation(ARContentModel model)
    {
        Animations.Add(model);
    }

    public void ClearAllLocalFiles()
    {
        if (DataSetXMLExist)
        {
            File.Delete(DataSetXMLPath);
        }
        if (DataSetDatExist)
        {
            File.Delete(DataSetDatPath);
        }
        if (ImageExist)
        {
            File.Delete(ImagePath);
        }
        if (MapExist)
        {
            File.Delete(MapPath);
        }
    }

    public void SetDataSet(Vuforia.DataSet dataSet)
    {
        DataSet = dataSet;
    }

    public bool CheckAllAnimationsPresent()
    {
        bool present = true;
        foreach (var animation in Animations)
        {
            present &= animation.BundleExist;
        }
        return present;
    }

    

}
