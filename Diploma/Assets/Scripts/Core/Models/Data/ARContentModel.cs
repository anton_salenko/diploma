﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;
using System.Globalization;

public class ARContentModel
{
    public const string COLLECTION_ID_KEY = "collection";
    public const string BUNDLE_ANDROID_URL_KEY = "bundle_android";
    public const string BUNDLE_IOS_URL_KEY = "bundle_ios";
    public const string ID_KEY = "id";
    public const string VERSION_KEY = "version";
    public const string NAME_KEY = "name";
    public const string SCALE_KEY = "scale";
    public const string MAP_X_KEY = "map_x";
    public const string MAP_Y_KEY = "map_y";
    public const string MAP_LABLE_KEY = "map_lable";

    public string BundlePath { get { return Path.Combine(CacheDirectoryPath, ID.ToString()); } }
    public string BundleUrl { get; private set; }
    public int Version { get; private set; }
    public int ID { get; private set; }
    public int CollectionID { get; private set; }
    public float Scale { get; private set; }
    public string Name { get; private set; }
    public CollectionModel Collection { get; private set; } 
    public string CacheDirectoryPath {  get { return Collection != null ? Path.Combine(Collection.CacheDirectoryPath, ID.ToString()) : null; } }
    public bool BundleExist { get { return File.Exists(BundlePath); } }
    public Vector2 MapPosition { get; private set; }
    public string MapLable { get; private set; }
    public bool HasPlayed { get; set; } = false;

    public ARContentModel(JSONNode json)
    {
#if UNITY_ANDROID || UNITY_EDITOR
        BundleUrl = NetworkRequests.ServerUrl + json[BUNDLE_ANDROID_URL_KEY];
#elif UNITY_IOS
        BundleUrl = NetworkRequests.ServerUrl + json[BUNDLE_IOS_URL_KEY];
#endif
        Version = json[VERSION_KEY].AsInt;
        CollectionID = json[COLLECTION_ID_KEY].AsInt;
        ID = json[ID_KEY].AsInt;
        Scale = json[SCALE_KEY].AsFloat;
        Name = json[NAME_KEY];
        float xPos, yPos;

        var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
        culture.NumberFormat.NumberDecimalSeparator = ".";
        float number = float.Parse("0.54", culture);
        xPos = float.Parse(json[MAP_X_KEY].ToString().Replace("\"", "").Replace("\"", ""), culture);
        yPos = float.Parse(json[MAP_Y_KEY].ToString().Replace("\"", "").Replace("\"", ""), culture);

        MapPosition = new Vector2(xPos, yPos);
        MapLable = json[MAP_LABLE_KEY];
    }

    public void SetCollection(CollectionModel book)
    {
        Collection = book;
    }

    public void ClearAllLocalFiles()
    {
        if (BundleExist)
        {
            File.Delete(BundlePath);
        }
    }

}
