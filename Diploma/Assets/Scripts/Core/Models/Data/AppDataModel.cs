﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AppDataModel
{

    private Dictionary<int, CollectionModel> books = new Dictionary<int, CollectionModel>();
    private Dictionary<int, ARContentModel> animations = new Dictionary<int, ARContentModel>();

    public Dictionary<int, CollectionModel> Books { get { return books; } }
    public Dictionary<int, ARContentModel> Animations { get { return animations; } }

    public CollectionModel SelectedCollection { get; private set; }
    public Vuforia.TrackableBehaviour openedAnimation { get; set; }
    public AssetBundle assetBundle;

    public static readonly string TEST_MARKERS_XML_PATH = Path.Combine(Application.streamingAssetsPath, "test_markers.xml");
    public static bool isDebug = false;

    public void SetBooks(IEnumerable<CollectionModel> books)
    {
        this.books.Clear();
        foreach (var book in books)
        {
            this.books.Add(book.ID, book);
        }
    }

    public void SetAnimations(IEnumerable<ARContentModel> animations)
    {
        this.animations.Clear();
        foreach (var animation in animations)
        {
            this.animations.Add(animation.ID, animation);
        }
    }

    public void SelectCollection(CollectionModel book)
    {
        SelectedCollection = book;
    }





}
