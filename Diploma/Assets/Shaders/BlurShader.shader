﻿
Shader "Unlit/BlurShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Power("Power", Range(1, 2)) = 1
		_Offset ("Offset", Range(1, 32)) = 1
	}

	SubShader
	{
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float2 _MainTex_TexelSize;
			fixed _Offset;
			fixed _Power;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				half4 offset = _MainTex_TexelSize.xxxy * half4(1, 2, 3, 0) * _Offset;
				half4 scale = half4(1, 1, 1, 1);
				scale.z *= (1.0 - 1.0 / _Power);
				scale.y *= scale.z * (1.0 - 1.0 / _Power);
				scale.x *= scale.y * (1.0 - 1.0 / _Power);

				fixed4 color = (tex2D(_MainTex, i.uv + offset.zw) * scale.x + tex2D(_MainTex, i.uv + offset.yw) * scale.y + tex2D(_MainTex, i.uv + offset.xw) * scale.z + tex2D(_MainTex, i.uv) + tex2D(_MainTex, i.uv - offset.xw) * scale.z + tex2D(_MainTex, i.uv - offset.yw) * scale.y + tex2D(_MainTex, i.uv - offset.zw) * scale.x) / (scale.x * 2 + scale.y * 2 + scale.z * 2 + scale.w);
				return fixed4(color.rgb, 1);
			}
			ENDCG
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float2 _MainTex_TexelSize;
			fixed _Offset;
			fixed _Power;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				half4 offset = _MainTex_TexelSize.yyyx * half4(1, 2, 3, 0) * _Offset;
				half4 scale = half4(1, 1, 1, 1);
				scale.z *= (1.0 - 1.0 / _Power);
				scale.y *= scale.z * (1.0 - 1.0 / _Power);
				scale.x *= scale.y * (1.0 - 1.0 / _Power);
				fixed4 color = (tex2D(_MainTex, i.uv + offset.wz) * scale.x + tex2D(_MainTex, i.uv + offset.wy) * scale.y + tex2D(_MainTex, i.uv + offset.wx) * scale.z + tex2D(_MainTex, i.uv) + tex2D(_MainTex, i.uv - offset.wx) * scale.z + tex2D(_MainTex, i.uv - offset.wy) * scale.y + tex2D(_MainTex, i.uv - offset.wz) * scale.x) / (scale.x * 2 + scale.y * 2 + scale.z * 2 + scale.w);
				return fixed4(color.rgb, 1);
			}
			ENDCG
		}
		
	}
	Fallback off
}
